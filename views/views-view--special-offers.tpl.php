<div class="<?php print $classes; ?>">
  <?php if ($rows): ?>
      <?php print $rows; ?>
  <?php elseif ($empty): ?>
      <?php print vermeer_var('special_offers_no_results'); ?>
  <?php endif; ?>
</div>
