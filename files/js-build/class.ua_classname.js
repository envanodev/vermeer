/****************************************************************
	UA Parser class
****************************************************************/
var ua_classname = ua_classname || {};
ua_classname = function ( properties ) {
	var _parser = new UAParser();
	_parser.setUA(navigator.userAgent);
	
	function _to_classname(input)
	{
		input = input.replace(/ /g,"_");
		input = input.replace(/\./g,"_");
		input = input.toLowerCase();
		return input;
	}
	
	//https://gist.github.com/padolsey/527683
	function _ie()
	{
		var ie = (function() {
			var v = 3, div = document.createElement("div"), a = div.all || [];
			while (div.innerHTML = "<!--[if gt IE "+(++v)+"]><br><![endif]-->", a[0]);
			return v > 4 ? v : !v;
		}());
		
		if(ie) { _addclass("ie" + ie); }
	}
	
	//https://github.com/faisalman/ua-parser-js
	function _os()
	{
		var os = _parser.getResult();
		_addclass(_to_classname(os.os.name + "_" + os.os.version));
		_addclass(_to_classname(os.os.name));
	}
	
	function _browser()
	{
		var browser = _parser.getBrowser();
		_addclass(_to_classname(browser.name + "_" + browser.version));
		_addclass(_to_classname(browser.name));
	}
	
	function _device()
	{
		var device = _parser.getDevice();
		if(typeof device.model != "undefined") { _addclass(_to_classname(device.model)); }
	}
	
	function _addclass(classname)
	{
		$("html").addClass(classname);
	}
	
	function _exceptions()
	{
		var browser = _parser.getBrowser();
		var os = _parser.getResult();

		//alert(browser.name + " " + parseInt(browser.version) + " " + os.os.name + " " + os.os.version)		
				
		/*
		Trapping for 
		iOS
		Stock Android browser on Android 2.3 or under.
		Amazon Kindle 1 and under
		*/
		if(
		browser.name.indexOf("Mobile Safari") != -1 && os.os.name.indexOf("iOS") != -1
		||
		browser.name.indexOf("Mobile Safari") != -1 && parseInt(os.os.name) < 3
		||
		browser.name.indexOf("Silk") != -1 && parseInt(browser.version) < 2
		)
		{
			_addclass("no-fixed-header");
			//alert("no fixed")
		} else {
			_addclass("fixed-header");
			//alert("fixed")
		}
	}

	return {
		init: function() {
			_ie();
			_os();
			_browser();
			_device();
			_exceptions();
		}
	};
};