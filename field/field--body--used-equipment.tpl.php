<?php
/**
 * @file field.tpl.php
 * Template for used equipment body overview
 * 
 */
?>

<div class="margin-right-2x <?php print $classes; ?>"<?php print $attributes; ?>>
	<h2 class="heading">Overview</h2>
    <?php foreach ($items as $delta => $item) { ?>
        <?php print render($item); ?>
    <?php } ?>
</div>
