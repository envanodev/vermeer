<?php
	
	/* 
		template for http://demo.new.vermeerdealersystem.com/new-equipment/agriculture 	
		
	*/	
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clear fix"<?php print $attributes; ?>>

    <?php // Latest Job Story ?>
    <?php if (isset($_GET['resource']) && $_GET['resource'] == 'jobstory'): ?>
        <?php if($node->field_job_story_headline['und'][0]['value']) { echo '<h1>' . $node->field_job_story_headline['und'][0]['safe_value'] . '</h1>'; } ?>
        <?php if($node->field_job_story_copy['und'][0]['value']) { echo $node->field_job_story_copy['und'][0]['value']; } ?>
        <?php if($node->field_job_story_link['und'][0]['url']) { $url = explode(';', $node->field_job_story_link['und'][0]['url']); echo '<p><a href="' . $url[0] . '">' . t('Download Job Story') . '</a></p>'; } ?>
        <p><a href="<?php echo parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH); ?>">&#8249; <?php echo t('Back to') . ' ' . $node->title; ?></a></p>
    <?php // Latest In the News ?>
    <?php elseif (isset($_GET['resource']) && $_GET['resource'] == 'inthenews'): ?>
        <?php if($node->field_in_the_news_headline['und'][0]['value']) { echo '<h1>' . $node->field_in_the_news_headline['und'][0]['safe_value'] . '</h1>'; } ?>
        <?php if($node->field_in_the_news_copy['und'][0]['value']) { echo $node->field_in_the_news_copy['und'][0]['value']; } ?>
        <?php if($node->field_in_the_news_link['und'][0]['url']) { $url = explode(';', $node->field_in_the_news_link['und'][0]['url']); echo '<p><a href="' . $url[0] . '">' . t('Download In the News Article') . '</a></p>'; } ?>
        <p><a href="<?php echo parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH); ?>">&#8249; <?php echo t('Back to') . ' ' . $node->title; ?></a></p>
    <?php // Latest News Release ?>
    <?php elseif (isset($_GET['resource']) && $_GET['resource'] == 'newsrelease'): ?>
        <?php if($node->field_news_release_headline['und'][0]['value']) { echo '<h1>' . $node->field_news_release_headline['und'][0]['safe_value'] . '</h1>'; } ?>
        <?php if($node->field_news_release_copy['und'][0]['value']) { echo $node->field_news_release_copy['und'][0]['value']; } ?>
        <?php if($node->field_news_release_link['und'][0]['url']) { $url = explode(';', $node->field_news_release_link['und'][0]['url']); echo '<p><a href="' . $url[0] . '">' . t('Download News Release') . '</a></p>'; } ?>
        <p><a href="<?php echo parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH); ?>">&#8249; <?php echo t('Back to') . ' ' . $node->title; ?></a></p>
    <?php else: ?>

    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <div class="content"<?php print $content_attributes; ?>>
	    <h3 class="heading lined margin-top-none"><?php echo(drupal_get_title()); ?></h3>
        <?php unset($content['links']); ?>
        <?php print render($content); ?>
    </div>

    <?php endif; ?>

</div>
