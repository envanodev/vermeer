<?php
/**
 * @file field--field_used_equipment_price.tpl.php
 * Template for used equipment price fields.
 */
?>

<div class="field_offers_short_desc">
<?php foreach ($items as $delta => $item): ?>
    <p>
        <?php echo render($item); ?>
    </p>
<?php endforeach; ?>
</div>


