/****************************************************************
	http://stackoverflow.com/questions/4216036/jquery-set-height-to-every-2-or-3-elements
****************************************************************/
jQuery.fn.setMinHeight = function(setCount) {
	if(setCount == 0)
	{
		setCount = this.length;
	}

	for(var i = 0; i < this.length; i+=setCount) {
		var curSet = this.slice(i, i+setCount), 
			height = 0;
		curSet.each(function() {
			jQuery(this).evenIfHidden( function(element) {
				height = Math.max(height, element.height())+1; //IE9 bug, missing 1px
			});
		})
		.css("min-height", height);
	}
	return this;
};