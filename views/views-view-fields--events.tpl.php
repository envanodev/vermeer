<?php

    $date = $row->field_field_event_date_range_1[0]['raw'];
    $start = explode(' ', $date['value']);
    $end = explode(' ', $date['value2']);
    $multiday = false;
    if ($start[0] != $end[0]) {
        $multiday = true;
    }

?>

<div class="event-block-container event-dates<?php if($multiday): ?> multiday<?php endif; ?>">
<?php echo $fields['field_event_date_range_1']->content; ?>
</div>
<div class="overflow padding-2x-all event-info<?php if($multiday): ?> multiday<?php endif; ?>">
<?php echo $fields['title']->wrapper_prefix; ?>
    <?php echo $fields['title']->content; ?>
<?php echo $fields['title']->wrapper_suffix; ?>
<?php echo $fields['field_event_location']->wrapper_prefix; ?>
    <?php echo $fields['field_event_location']->content; ?>
<?php echo $fields['field_event_location']->wrapper_suffix; ?>
<?php echo $fields['field_event_category']->wrapper_prefix; ?>
    <?php echo $fields['field_event_category']->content; ?>
<?php echo $fields['field_event_category']->wrapper_suffix; ?>
</div>
