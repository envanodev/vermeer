<?php
/**
 * @file field--field_used_equipment_price.tpl.php
 * Template for used equipment price fields.
 */
?>

	<h2 class="heading margin-top-2x clear"><?php echo drupal_get_title(); ?></h2>
    <?php foreach ($items as $delta => $item) { ?>
      <h2 class="heading price margin-bottom-2x <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
        <?php echo (render($item) > 0 ? theme('price', array('price' => render($item))) : vermeer_var_get('call_for_price')); ?>
      </h2>
    <?php } ?>
    
    

