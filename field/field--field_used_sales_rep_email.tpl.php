<?php
/**
 * @file field.tpl.php
 * Template for used equipment sales rep.
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
        <a href="mailto:<?php print render($item); ?>"><?php print render($item); ?></a>
      </div>
    <?php endforeach; ?>
</div>
