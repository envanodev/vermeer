<div id="block-views-events-calendar-block-1" class="block block-views">
	<div class="content">
		<div class="view view-events-calendar view-id-events_calendar view-display-id-block_1 view-dom-id-edc73ff90c46dfc946fbed89f3d404aa jquery-once-1-processed">
			<div class="view-header">
				<div class="date-nav-wrapper clearfix">
					<div class="date-nav item-list">
						<div class="date-heading">
							<h3><a href="http://vermeermidwest.com/events?date=2014-12&amp;mini=" title="View full page month">December</a></h3>
						</div>
						<ul class="pager">
							<li class="date-prev"> <a href="http://vermeermidwest.com/events?mini=2014-11" title="Navigate to previous month" rel="nofollow">«</a> </li>
							<li class="date-next"> <a href="http://vermeermidwest.com/events?mini=2015-01" title="Navigate to next month" rel="nofollow">»</a> </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="view-content">
				<div class="calendar-calendar">
					<div class="month-view">
						<table class="mini">
							<thead>
								<tr>
									<th class="days sun"> S </th>
									<th class="days mon"> M </th>
									<th class="days tue"> T </th>
									<th class="days wed"> W </th>
									<th class="days thu"> T </th>
									<th class="days fri"> F </th>
									<th class="days sat"> S </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="events_calendar-2014-11-30" class="sun mini empty"><div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-01" class="mon mini past has-no-events"><div class="month mini-day-off"> 1 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-02" class="tue mini past has-no-events"><div class="month mini-day-off"> 2 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-03" class="wed mini past has-no-events"><div class="month mini-day-off"> 3 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-04" class="thu mini past has-no-events"><div class="month mini-day-off"> 4 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-05" class="fri mini past has-no-events"><div class="month mini-day-off"> 5 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-06" class="sat mini past has-no-events"><div class="month mini-day-off"> 6 </div>
										<div class="calendar-empty">&nbsp;</div></td>
								</tr>
								<tr>
									<td id="events_calendar-2014-12-07" class="sun mini past has-no-events"><div class="month mini-day-off"> 7 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-08" class="mon mini past has-no-events"><div class="month mini-day-off"> 8 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-09" class="tue mini past has-no-events"><div class="month mini-day-off"> 9 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-10" class="wed mini today has-no-events"><div class="month mini-day-off"> 10 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-11" class="thu mini future has-no-events"><div class="month mini-day-off"> 11 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-12" class="fri mini future has-no-events"><div class="month mini-day-off"> 12 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-13" class="sat mini future has-no-events"><div class="month mini-day-off"> 13 </div>
										<div class="calendar-empty">&nbsp;</div></td>
								</tr>
								<tr>
									<td id="events_calendar-2014-12-14" class="sun mini future has-no-events"><div class="month mini-day-off"> 14 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-15" class="mon mini future has-no-events"><div class="month mini-day-off"> 15 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-16" class="tue mini future has-no-events"><div class="month mini-day-off"> 16 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-17" class="wed mini future has-no-events"><div class="month mini-day-off"> 17 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-18" class="thu mini future has-no-events"><div class="month mini-day-off"> 18 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-19" class="fri mini future has-no-events"><div class="month mini-day-off"> 19 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-20" class="sat mini future has-no-events"><div class="month mini-day-off"> 20 </div>
										<div class="calendar-empty">&nbsp;</div></td>
								</tr>
								<tr>
									<td id="events_calendar-2014-12-21" class="sun mini future has-no-events"><div class="month mini-day-off"> 21 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-22" class="mon mini future has-no-events"><div class="month mini-day-off"> 22 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-23" class="tue mini future has-no-events"><div class="month mini-day-off"> 23 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-24" class="wed mini future has-no-events"><div class="month mini-day-off"> 24 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-25" class="thu mini future has-no-events"><div class="month mini-day-off"> 25 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-26" class="fri mini future has-no-events"><div class="month mini-day-off"> 26 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-27" class="sat mini future has-no-events"><div class="month mini-day-off"> 27 </div>
										<div class="calendar-empty">&nbsp;</div></td>
								</tr>
								<tr>
									<td id="events_calendar-2014-12-28" class="sun mini future has-no-events"><div class="month mini-day-off"> 28 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-29" class="mon mini future has-no-events"><div class="month mini-day-off"> 29 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-30" class="tue mini future has-no-events"><div class="month mini-day-off"> 30 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2014-12-31" class="wed mini future has-no-events"><div class="month mini-day-off"> 31 </div>
										<div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2015-01-01" class="thu mini empty"><div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2015-01-02" class="fri mini empty"><div class="calendar-empty">&nbsp;</div></td>
									<td id="events_calendar-2015-01-03" class="sat mini empty"><div class="calendar-empty">&nbsp;</div></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
