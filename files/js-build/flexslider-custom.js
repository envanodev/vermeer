$(window).load(function() {
	
	//https://github.com/woothemes/FlexSlider/issues/292
	$(".flexslider[data-flexslider-parameters]").each(function(index, element) {
		var t = $(this);
		var parameters = t.attr("data-flexslider-parameters");
		try {
			var options = JSON.parse(parameters);			
			
			//Runs right away
			if(options.hasOwnProperty("run_function")){
				var init_function = window[options["run_function"]];
				if(typeof init_function === "function") {					
					init_function(t);					
				}
			}
			
			//Init
			if(options.hasOwnProperty("init_function")){
				var init_function = window[options["init_function"]];
				if(typeof init_function === "function") {
					options["init"] = function(slider){
						init_function(t);
					}
				}
			}
			
			//Start-first slide
			if(options.hasOwnProperty("start_function")){
				var start_function = window[options["start_function"]];
				if(typeof start_function === "function") {
					options["start"] = function(slider){
						start_function(t);
					}
				}
			}
			
			//After slide
			if(options.hasOwnProperty("after_function")){
				var after_function = window[options["after_function"]];
				if(typeof after_function === "function") {
					options["after"] = function(slider){
						after_function(t);
					}
				}
			}
			
			t.flexslider(options);
		}
		catch(err) {
			console.log(err);
		}
	});	
	
});

function splash_slider_run(slider)
{
	slider.find("li").setMinHeight(0);
	if(typeof _gaq != "undefined") _gaq.push(['_trackEvent', 'slider', 'view', 1, 1]);	
}

function splash_slider_after(slider)
{
	if(typeof _gaq != "undefined") _gaq.push(['_trackEvent', 'slider', 'view', slider.currentSlide+1, 1]);
}

function used_slider_start(slider)
{
	var h = slider.find(".flex-viewport").height();
	slider.find(".equipment-box").css("min-height",h);
}