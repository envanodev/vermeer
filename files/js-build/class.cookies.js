var class_cookies = class_cookies || {};
class_cookies = function ( merge_options ) {
	//Default Private Properties
	var defaults = {
		expires_as_days: 365,
		path: "/",
		name: "",
		value: ""
	}

	//Merge external options with defaults - Example usage options.someproperty;
	var options = $.extend(defaults, merge_options);

	//Private Methods
	function _convert_days(days)
	{
		return days * 24 * 60 * 60 * 1000;
	}

	//Public properties and methods.
	return {
		create_cookie: function() {
			var date = new Date();
			date.setTime( date.getTime()+ _convert_days(options.expires_as_days) );
			var expires = "; expires=" + date.toGMTString();

			document.cookie = options.name + "=" + options.value + expires + "; path=" + options.path;
		},

		read_cookie: function() {
			var cookie_equals = options.name + "=";
			var cookie_array = document.cookie.split(";");

			for(var i=0; i < cookie_array.length; i++)
			{
				var name_value = cookie_array[i];

				while(name_value.charAt(0) == " ") 
				{
					name_value = name_value.substring(1,name_value.length);
				}
				
				if(name_value.indexOf(cookie_equals) == 0)
				{
					return name_value.substring(cookie_equals.length,name_value.length);
				}
			}
			return null;
		},
		
		erase_cookie: function() {
			options.expires_as_days = -1;
			options.value = "";
			this.create_cookie();
		}
	};
};