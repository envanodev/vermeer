<?php
/**
 * @file field--field_used_equipment_price.tpl.php
 * Template for used equipment price fields.
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php if (!$label_hidden): ?>
        <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
    <?php endif; ?>
    <div class="field-items"<?php print $content_attributes; ?>>
        <?php foreach ($items as $delta => $item): ?>
            <?php if ($offer = vermeer_api_special_offer_load($item['#markup'])): ?>
            <img src="<?php echo $offer->field_offers_img_source['und'][0]['value']; ?>" />
            <h3><?php echo $offer->title; ?></h3>
            <p><?php echo render(field_view_field('node', $offer, 'field_offers_short_desc')); ?></p>
            <p><a href="/<?php echo vermeer_var_lookup_path('alias', 'node/' . $offer->nid); ?>"><?php echo t('Read More'); ?> &raquo;</a></p>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
