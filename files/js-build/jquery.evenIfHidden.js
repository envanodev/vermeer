/****************************************************************
	https://github.com/piotrooo/jquery-evenifhidden
****************************************************************/
jQuery.fn.evenIfHidden = function( callback ) {

	return this.each( function() {
		var self = jQuery(this);
		var styleBackups = [];

		var hiddenElements = self.parents().addBack().filter(":hidden"); //var hiddenElements = self.parents().andSelf().filter(':hidden');

		if ( ! hiddenElements.length ) {
			callback( self );
			return true; //continue the loop
		}

		hiddenElements.each( function() {
			var style = $(this).attr("style");
			style = typeof style == "undefined"? "": style;
			styleBackups.push( style );
			$(this).attr( "style", style + " display: block !important;" );
		});

		hiddenElements.eq(0).css( "left", -10000 );

		callback(self);

		hiddenElements.each( function() {
			$(this).attr( "style", styleBackups.shift() );
		});
	
	});
};