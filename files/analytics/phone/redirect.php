<?php
//NOTE, GOOGLE ANALYTICS FILTERS OUT ENVANO'S IP
//http://code.google.com/p/php-ga/

include("autoload.php");

use UnitedPrototype\GoogleAnalytics;

// Initilize GA Tracker
$tracker = new GoogleAnalytics\Tracker('UA-00000000-1', 'www.domain.com');

// Assemble Visitor information
// (could also get unserialized from database)
$visitor = new GoogleAnalytics\Visitor();
$visitor->setIpAddress($_SERVER['REMOTE_ADDR']);
$visitor->setUserAgent($_SERVER['HTTP_USER_AGENT']);
//$visitor->setScreenResolution('1024x768');

// Assemble Session information
// (could also get unserialized from PHP session)
$session = new GoogleAnalytics\Session();

// Assemble Page information
#$page = new GoogleAnalytics\Page('/find-dealer.php');
#$page->setTitle('Find a Dealer | Vermeer');

// Track page view
#$tracker->trackPageview($page, $session, $visitor);

$event = new GoogleAnalytics\Event("Outbound Links","Phone","Mobile");

$tracker->trackEvent($event, $session, $visitor);

//echo "no error";

header("Location: tel:" . $_GET["tel"]);
?>