<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <h3 class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</h3>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
        <?php $address = render($item); echo $address; $address_string = urlencode(strip_tags($address)); ?>
		<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDVCD-rPK_y4nLWltFEiHKBUks4jRvU6rI&q=<?php echo $address_string; ?>" width="100%" height="450" frameborder="0" style="border:0"></iframe>      </div>
    <?php endforeach; ?>
  </div>
</div>
