<?php
	//Template used for the find a sales rep form.
	//NOTE: this is the form on the find sales rep page, not the used equipment page	
	
	
?>
<?php if (!empty($q)){ ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
    
    
  ?>
<?php } ?>
    <?php foreach ($widgets as $id => $widget) { ?>
        <?php if (!empty($widget->label)){ ?>
         <div class="margin-bottom-half">
	         <label for="edit-zipcode" data-font-size="+1">
            <?php print $widget->label; ?>
	         </label>
         </div>
        <?php } ?>
        <div class="group">
        <?php if (!empty($widget->operator)) { ?>
            <?php print $widget->operator; ?>
        <?php } ?>
          <?php print $widget->widget; ?>
    <?php } ?>
    <?php if (!empty($sort_by)) { ?>
      <div>
        <?php print $sort_by; ?>
      </div>
      <div>
        <?php print $sort_order; ?>
      </div>
    <?php } ?>
    <?php if (!empty($items_per_page)){ ?>
      <div>
        <?php print $items_per_page; ?>
      </div>
    <?php } ?>
    <?php if (!empty($offset)){ ?>
      <div>
        <?php print $offset; ?>
      </div>
    <?php } ?>
    <div>
      <?php print $button; ?>
    </div>
    <?php if (!empty($reset_button)){ ?>
    <div>
    <p class="align-center">
        <?php print $reset_button; ?>
    </p>
    </div>
    <?php } ?>
        </div>
