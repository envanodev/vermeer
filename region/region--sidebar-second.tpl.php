<?php 
	//This is the side bar that appears to the right of internal pages
	$current_path = explode('/', urldecode(request_uri()));
	if(!empty($current_path[1])) {
		$current_path = $current_path[1];
	} else {
		$current_path = '';
	}
	
?>
<aside class="fixed <?php print($classes) ?>">
	<?php if($current_path == 'new-equipment') { ?>
	<h3 class="heading lined margin-top-none">Industries</h3>
	<?php } else { ?>
	<h3 class="heading lined margin-top-none"><?php print(drupal_get_title()); ?></h3>
	<?php } print $content ?>
</aside>