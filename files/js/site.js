//Uses Prepros to minify & concatenate - http://alphapixels.com/prepros/

//@prepros-append ..\js-build\common.js

//@prepros-append ..\js-build\flexslider-custom.js

//@prepros-append ..\js-build\class.responsive.js
//@prepros-append ..\js-build\class.ua_classname.js
//@prepros-append ..\js-build\class.cookies.js

//@prepros-append ..\js-build\jquery.flexslider.js
//@prepros-append ..\js-build\jquery.setminheight.js
//@prepros-append ..\js-build\jquery.evenifhidden.js
//@prepros-append ..\js-build\jquery.fitvids.js
//@prepros-append ..\js-build\jquery.unveil.js
//@prepros-append ..\js-build\jquery.querystring.js

//@prepros-append ..\js-build\ios-orientationchange-fix.js
//@prepros-append ..\js-build\ua-parser.js
//@prepros-append ..\js-build\fastclick.js
//@prepros-append ..\js-build\fragmention.js
//@prepros-append ..\js-build\modernizr.js