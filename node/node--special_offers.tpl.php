<?php
    // Video
    if(isset($node->field_offers_video_url['und'][0])) {
        $video = $node->field_offers_video_url['und'][0]['value'];
    }
    elseif(isset($node->field_offers_video_source['und'][0])) {
        $video = $node->field_offers_video_source['und'][0]['value'];
    }
    else {
        $video = NULL;
    }
    if($video) {
        $youtube_id = vermeer_youtube_parse($video);
    }
    
    // Video Caption
    if(isset($node->field_offers_video_caption['und'][0])) {
        $video_caption = $node->field_offers_video_caption['und'][0]['value'];
    }
?>


<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clear fix"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php // Expiration Date and Image ?>
	<?php if(!empty($content['field_offers_dealer_img']['#object']->field_offers_dealer_img)) { ?>
    <?php 
	    	$imgPath = image_style_url('event_full', $content['field_offers_dealer_img']['#object']->field_offers_dealer_img['und'][0]['uri']);
	?>
			<img src="<?php echo($imgPath); ?>" class="full-width" />
    <?php } ?>
    <?php print render($content['field_offers_expiration_date']); ?>
    <?php if (isset($content['field_offers_img_source'][0]['#markup']) && $url = $content['field_offers_img_source'][0]['#markup']): ?>
        <div class="field-name-field-offers-dealer-img field-type-image">
            <img src="<?php echo $url; ?>" class="full-width" />
        </div>
    <?php endif; ?>

    <?php // Body or Short Description field ?>
    <?php if(isset($content['body'])): ?>
        <?php print render($content['body']); ?>
    <?php else: ?>
        <?php print render($content['field_offers_short_desc']); ?>
    <?php endif; ?>
    
   <?php // Video ?>
    <?php if ($video): ?>
		<div class="watch-video-container">
		    <?php if (isset($youtube_id)): ?>
		        <iframe class="full-width" height="400" src="http://www.youtube.com/embed/<?php echo (isset($youtube_id) ? $youtube_id : ''); ?>" frameborder="0"></iframe>
		    <?php elseif ($video): ?>
		            <p class="video-url"><?php echo $video; ?></p>
		            <div class="video-container" id="special-offer-media-video-<?php echo $row->_field_data['nid']['entity']->nid; ?>"></div>
		    <?php endif; ?>
		    <?php echo (isset($video_caption) ? '<p class="caption">' . $video_caption . '</p>' : '') ?> 
		</div>
        <p class="video"><a class="new-window-ignore"  rel="shadowbox[gallery]" href="http://www.youtube.com/embed/<?php echo (isset($youtube_id) ? $youtube_id : ''); ?>"><?php echo t('Watch Video'); ?> &#187;</a></p>
		
    <?php endif ?>
  </div>
</div>
