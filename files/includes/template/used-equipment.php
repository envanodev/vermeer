			<div class="flexslider used-equipment add-overthrow" data-flexslider-theme="black" data-flexslider-arrows="outside" data-flexslider-parameters='{
				"animation": "slide",
				"animationSpeed": 400,
				"slideshow": false,
				"smoothHeight": false,
				"controlNav": false,
				"directionNav": true,
				"useCSS": true,
				"touch": false,
				"prevText": "",
				"nextText": "",
				"itemWidth": 230,
				"minItems": 1,
				"maxItems": 4,
				"start_function": "used_slider_start"
			}'>
				<ul class="slides">
					<li>
						<div class="padding-2x left right">
							<div class="equipment-box">
								<a href="used-equipment-single.php" data-hover-effect="black"><img src="http://placehold.it/213x156" class="full-width" alt="2012 Vermeer D24X40II Directional Drills"></a>
								<div class="padding-2x-all">
									<h3 class="heading margin-top-none margin-bottom-half"><a href="used-equipment-single.php" class="inherit hover-lighten">2012 Vermeer D24X40II Directional Drills</a></h3>
									<p class="font-color-medium" data-font-size="-1"><b>Hours:</b> 1315  |  <b>Horsepower:</b> 125</p> <!-- Optional -->
									<h3 class="heading price margin-bottom-none">$169,900 <sup class="valign-middle">USD</sup></h3>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="padding-2x left right">
							<div class="equipment-box">
								<a href="used-equipment-single.php" data-hover-effect="black"><img src="http://placehold.it/213x156" class="full-width" alt="2012 Vermeer D24X40II Directional Drills"></a>
								<div class="padding-2x-all">
									<h3 class="heading margin-top-none margin-bottom-half"><a href="used-equipment-single.php" class="inherit hover-lighten">2012 Vermeer D24X40II Directional Drills Long Title That Rambles On For A Sentence</a></h3>
									<p class="font-color-medium" data-font-size="-1"><b>Hours:</b> 1315  |  <b>Horsepower:</b> 125</p> <!-- Optional -->
									<h3 class="heading price margin-bottom-none">$169,900 <sup class="valign-middle">USD</sup></h3>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="padding-2x left right">
							<div class="equipment-box">
								<a href="used-equipment-single.php" data-hover-effect="black"><img src="http://placehold.it/213x156" class="full-width" alt="2012 Vermeer D24X40II Directional Drills"></a>
								<div class="padding-2x-all">
									<h3 class="heading margin-top-none margin-bottom-half"><a href="used-equipment-single.php" class="inherit hover-lighten">2012 Vermeer D24X40II</a></h3>
									<h3 class="heading price margin-bottom-none">$169,900 <sup class="valign-middle">USD</sup></h3>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="padding-2x left right">
							<div class="equipment-box">
								<a href="used-equipment-single.php" data-hover-effect="black"><img src="http://placehold.it/213x156" class="full-width" alt="2012 Vermeer D24X40II Directional Drills"></a>
								<div class="padding-2x-all">
									<h3 class="heading margin-top-none margin-bottom-half"><a href="used-equipment-single.php" class="inherit hover-lighten">2012 Vermeer D24X40II Directional Drills</a></h3>
									<p class="font-color-medium" data-font-size="-1"><b>Hours:</b> 1315</p> <!-- Optional -->
									<h3 class="heading price margin-bottom-none">$169,900 <sup class="valign-middle">USD</sup></h3>
								</div>
							</div>	
						</div>
					</li>
					<li>
						<div class="padding-2x left right">
							<div class="equipment-box">
								<a href="used-equipment-single.php" data-hover-effect="black"><img src="http://placehold.it/213x156" class="full-width" alt="2012 Vermeer D24X40II Directional Drills"></a>
								<div class="padding-2x-all">
									<h3 class="heading margin-top-none margin-bottom-half"><a href="used-equipment-single.php" class="inherit hover-lighten">2012 Vermeer D24X40II Directional Drills</a></h3>
									<p class="font-color-medium" data-font-size="-1"><b>Horsepower:</b> 125</p> <!-- Optional -->
									<h3 class="heading price margin-bottom-none">$169,900 <sup class="valign-middle">USD</sup></h3>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>