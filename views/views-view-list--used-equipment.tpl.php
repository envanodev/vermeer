<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 * 
 * NOTES: 
 * This template file is for the used equipment items on the /used-equipment/ overview page.
 * 
 *
 *
 */
?>

<?php
	$counter = 0;	
?>
<ul class="columns columns-3 children-margin-bottom-2x" data-bp="mobilelandscape" data-equal-height=".used-equipment-equal-height" data-equal-height-nth="3">
<?php foreach ($rows as $id => $row) { ?>
	  	<li class="<?php if(!($counter % 3)) { echo('column-row'); } ?>">
	  		<article class="equipment-box group">
	  	<?php print $row; ?>
	  		</article>
	  	</li>
<?php 
		$counter ++;
	}
	
	
?>
</ul>
