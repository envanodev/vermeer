
<?php if (!empty($q)){ ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php } ?>
<div class="used-equipment-rep">
    <?php foreach ($widgets as $id => $widget) { ?>
    
        <?php if (!empty($widget->label)){ ?>
         <h3 class="heading lined margin-top-none margin-bottom-2x">
            <?php  
	            if(!empty(vermeer_var('sales_rep_search_label'))) {
		            print(vermeer_var('sales_rep_search_label')); 
	            } else {
		             print $widget->label;
	            }
	            
	            
	         ?>
          </h3>
        <?php } ?>
        <?php if (!empty($widget->operator)) { ?>
            <?php print $widget->operator; ?>
        <?php } ?>
          <?php print $widget->widget; ?>
    <?php } ?>
    <?php if (!empty($sort_by)) { ?>
      <div>
        <?php print $sort_by; ?>
      </div>
      <div>
        <?php print $sort_order; ?>
      </div>
    <?php } ?>
    <?php if (!empty($items_per_page)){ ?>
      <div>
        <?php print $items_per_page; ?>
      </div>
    <?php } ?>
    <?php if (!empty($offset)){ ?>
      <div>
        <?php print $offset; ?>
      </div>
    <?php } ?>
    <div>
      <?php print $button; ?>
    </div>
    <?php if (!empty($reset_button)){ ?>
    <div>
    <p class="align-center">
        <?php print $reset_button; ?>
    </p>
    </div>
    <?php } ?>
</div>