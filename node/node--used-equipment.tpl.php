<!-- <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>> -->

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
<!--  Used Equipment Item  -->
    <?php unset($content['links']); ?>
    <?php print render($content); ?>
<!--  End Used Equipment Item  -->
<!-- </div> -->
