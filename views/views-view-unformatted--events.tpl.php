<?php if (!empty($title)): ?>
    <h2 class="heading"><?php print $title; ?></h2>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
    <article <?php if ($classes_array[$id]) { print 'class="box bordered overflow margin-bottom-2x ' . $classes_array[$id] .' event"';  } ?>>
        <?php print $row; ?>
    </article>
<?php endforeach; ?>
