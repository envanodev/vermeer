<?php 
	require('/data/www/vermeerdealersystem.com/www/sites/all/themes/vermeer/_includes/mailgun-php/vendor/autoload.php');
	use Mailgun\Mailgun;
	
	


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
<title>Share Equipment</title>

<link rel="stylesheet" href="/sites/all/themes/vermeer/files/css/all.css" media="all">
<style type="text/css">
html
{
background-color: #fff;
height: 100%;
}
</style>
</head>

<body id="specificity">
	<main class="content padding-4x-all">
		<?php 
				if(!empty($_POST['name']) && !empty($_POST['recipient-email']) && !empty($_POST['name'])) {
 					$mg = new Mailgun("key-2g4vfhzkisk53a-818u2zjqc39cr2q54");
					$domain = "vermeerdealersystem.com";
					$to = $_POST['recipient-email'];
					$subject = $_POST['name'] . ' Has Shared: ' . $_POST['equipment-title'];
					$body =  $_POST['message'];
					$return = $mg->sendMessage($domain, array('from'    => 'No-Reply@vermeerdealersystem.com', 
				                                'to'      => $to, 
				                                'subject' => $subject, 
				                                'text'    => $body));
				                                
				                                
					if($return->http_response_code == 200) { ?>
						<div class="reduced margin-bottom-2x">
							<h3 class="heading margin-top-none margin-bottom-none">Your Email Has Been Sent</h3>
						</div>
					<?php } else {?>
						<div class="reduced margin-bottom-2x">
							<h3 class="heading margin-top-none margin-bottom-none">Error, Please Try Again Later. Thank-you!</h3>
						</div>
			<?php 			}	
			}
			
			
		?>
		
		<h2 class="heading margin-top-none margin-bottom-2x">Share Equipment</h2>		
		<form method="post" id="share-equipment" >
			<ul class="form">
				<li class="required">
					<label for="name">Your Name</label>
					<input type="text" name="name" id="name" class="full-width" required>
				</li>
				<li class="required">
					<label for="recipient-email">Recipient Email</label>
					<input type="email" name="recipient-email" id="recipient-email" class="full-width" required>
				</li>
				<li class="required">
					<label for="message">Message</label>
					<textarea name="message" id="message" class="full-width"><?php if(!empty($_GET['url']) && !empty($_GET['title'])) { echo($_GET['title'] . ' - ' .  $_GET['url']); } ?></textarea>
					<input type="hidden" name="equipment-title" value="<?php if(!empty($_GET['title'])) { echo($_GET['title']); } ?>" />
				</li>
				<li class="margin-top-3x">
					<button  type="submit"  class="button margin-bottom-2x margin-right-half submit-form" data-theme="yellow" data-size="small" tabindex="0" value="Send">Send</button>					
				</li>
			</ul>
		</form>
	</main>
</body>
</html>