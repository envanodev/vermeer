<?php 

$dates = explode(',', $output);
$time1 = strtotime($dates[0]);
$time2 = strtotime($dates[1]);
$start = date('j M', $time1);
$end = (isset($dates[1]) ? date('j M', $time2) : $start);
$alias = vermeer_var_lookup_path('alias', 'node/' . $row->nid);

if ($start != $end) {
    echo '<a class="inherit" href="' . $alias . '">';
    echo '<time class="event-block dtstart date" datetime="' . str_replace(' ', 'T', $dates[0]) . '">';
    echo '<span class="day">' . date('j', $time1) . '</span><span class="month">' . date('M', $time1) . '</span>';
    echo '</time>';
    echo '<span class="event-block event-time-span">-</span>'; 
    echo '<time class="event-block dtend date" datetime="' . str_replace(' ', 'T', $dates[1]) . '">';
    echo '<span class="day">' . date('j', $time2) . '</span><span class="month">' . date('M', $time2) . '</span>';
    echo '</time>';
    echo '</a>';
}
else {
    echo '<a class="inherit" href="' . $alias . '">';
    echo '<time class="event-block dtstart date" datetime="' . str_replace(' ', 'T', $dates[0]) . '">';
    echo '<span class="day">' . date('j', $time1) . '</span><span class="month">' . date('M', $time1) . '</span>';
    echo '</time>';
    echo '</a>';
}

?>
