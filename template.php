<?php

/* --------------------------
 * PREPROCESS THEME HOOKS
 * This file includes a lot of the functions used inside the theme. 
 * Functions that are used to render the menu, pages, and its also to add any scripts/styles needed. 
 * -------------------------- */

function vermeer_preprocess_html(&$variables){
    global $_domain;

    
    //old files to remove
    //drupal_add_css('_ui/libs/css/reset.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/libs/jquery/css/colorbox-1.3.6.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/_main.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/_shared.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/content.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/home.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/sales-rep.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/special-offers.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/new-equipment.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/used-equipment.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/events.css', array('group' => CSS_THEME));
    //drupal_add_css('_ui/skin/css/colorbox.css', array('group' => CSS_THEME));
    
    // Remove mobile css if No Mobile cookie is set
    // Additional stylesheet only for the portal domain
    if ($_domain['domain_id'] == 1) {
        //drupal_add_css('_ui/skin/css/portal.css', array('group' => CSS_THEME));
    } 

    // Add Javascript Libraries
    
    //old theme files REMOVE
	//drupal_add_js('_ui/skin/js/declare.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/reset.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/modernizr/2.0.6.min.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/equalheights.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/linkIcons.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/hoverIntent.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/bxSlider.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/client.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/swfobject/swfobject.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jwplayer/5.10/jwplayer.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/colorbox-1.3.16.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/libs/jquery/js/multiAccordion.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/skin/js/_main.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/skin/js/used-equipment.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/skin/js/new-equipment.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/skin/js/events.js', array('group' => JS_THEME));
	//drupal_add_js('_ui/skin/js/plugins.js', array('group' => JS_THEME));
	
    // Add Google Analytics tracking code
	$analytics_code = "";
    $analytics_id = vermeer_var('analytics_id');
    $analytics_enabled = vermeer_var('analytics_enabled');
        if ($analytics_id && $analytics_enabled) {
	        
            $analytics_code = <<<END
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', '$analytics_id', 'auto');
  ga('create', 'UA-57613201-1', 'auto', {'name': 'GlobalTracking'});
  ga('send', 'pageview');
  ga('GlobalTracking.send', 'pageview');
  
  
END;
      	drupal_add_js($analytics_code, 'inline');
        
        } else {
            $analytics_code = <<<END
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  
  ga('create', 'UA-57613201-1', 'auto', {'name': 'GlobalTracking'});
  ga('send', 'pageview');
  ga('GlobalTracking.send', 'pageview');
  
  
END;
	        
      	drupal_add_js($analytics_code, 'inline');
    }

    // Add translations vars to js
    $js_translations  = 'var vermeer_translation_expand_all = "' . t('Expand All') . '"; ';
    $js_translations .= 'var vermeer_translation_collapse_all = "' . t('Collapse All') . '"; ';
    $js_translations .= 'var vermeer_translation_view_all = "' . t('View All') . '"; ';
    $js_translations .= 'var vermeer_translation_view_15 = "' . t('View 15') . '"; ';
    $js_translations .= 'var vermeer_translation_price_high_low = "' . t('Price: High to Low') . '"; ';
    $js_translations .= 'var vermeer_translation_price_low_high = "' . t('Price: Low to High') . '"; ';
    $js_translations .= 'var vermeer_translation_set = "' . t('Set @current of @total') . '"; ';
    drupal_add_js($js_translations, 'inline');


    // Add SEO markup from custom module
    vermeer_seo_include_meta();
    
    // Load Theme Tab JS on New Equipment Lines and Model Pages
    if (arg(0) == 'new-equipment' && arg(2) != NULL) {
        $shadowboxInit = <<<END
			$(window).load(function(e) {
				Shadowbox.init({
					players: ["img","inline"]
				});
			});

END;
        drupal_add_js($shadowboxInit, 'inline');
        drupal_add_css('sites/all/modules/contrib/field_group/horizontal-tabs/horizontal-tabs.css',
          array('type' => 'file', 'scope' => 'header')
        );
        drupal_add_css('sites/all/themes/vermeer/files/libraries/shadowbox-custom/shadowbox.css',
          array('type' => 'file', 'scope' => 'header')
        );
        drupal_add_js('sites/all/modules/custom/vermeer_field_group/horizontal-tabs/horizontal-tabs.js',
          array('type' => 'file', 'scope' => 'header')
        );   
        drupal_add_js('sites/all/themes/vermeer/files/libraries/shadowbox-custom/shadowbox3.js',
          array('type' => 'file', 'scope' => 'header')
        );   
    }

    if (ctype_digit(arg(1)) && $node = node_load(arg(1))) {
        if ($node->type == 'home_page') {
            unset($variables['classes_array'][1]);
            $variables['classes_array'][] = 'front';
        }
    }
    
	 
    //New 2014 theme 
    drupal_add_js('sites/all/themes/vermeer/files/js/site.min.js', array('group' => JS_THEME));
    // Add Stylesheets
    drupal_add_css('sites/all/themes/vermeer/files/css/all.css', array('group' => CSS_THEME));
    drupal_add_css('sites/all/themes/vermeer/files/css/calendar.css', array('group' => CSS_THEME));
    
    if(user_is_logged_in()) {
   	 	drupal_add_css('sites/all/themes/vermeer/files/css/admin.css', array('group' => CSS_THEME));
    }
    if (isset($node) && $node->type == 'home_page') {
	    drupal_add_css('sites/all/themes/vermeer/files/css/index.css', array('group' => CSS_THEME));

    }
    if (isset($node) && $node->type == 'used_equipment') {
	    drupal_add_css('sites/all/themes/vermeer/files/libraries/shadowbox-custom/shadowbox.css', array('group' => CSS_THEME));
		drupal_add_js('sites/all/themes/vermeer/files/libraries/shadowbox-custom/shadowbox3.js', array('group' => JS_THEME));
        $shadowboxInit = <<<END
			$(window).load(function(e) {
				Shadowbox.init({
					players: ["img","inline"]
				});
			});

END;
            drupal_add_js($shadowboxInit, 'inline');

    }
    if (isset($node) && $node->type == 'landing_page') {
	    drupal_add_css('sites/all/themes/vermeer/files/libraries/shadowbox-custom/shadowbox.css', array('group' => CSS_THEME));
		drupal_add_js('sites/all/themes/vermeer/files/libraries/shadowbox-custom/shadowbox3.js', array('group' => JS_THEME));
        $shadowboxInit = <<<END
			$(window).load(function(e) {
				Shadowbox.init({
					players: ["img","inline"]
				});
			});

END;
            drupal_add_js($shadowboxInit, 'inline');

    }
    
    
    
    //site tour
	if($_domain['domain_id'] == 35) {
/*
		drupal_add_css('', array('group' => CSS_THEME));
		drupal_add_js('', array('group' => JS_THEME));
*/

		drupal_add_css(drupal_get_path('theme', 'vermeer') . '/files/js/demo-tour/joyride-2.1.css', array('group' => CSS_THEME));
		drupal_add_css(drupal_get_path('theme', 'vermeer') . '/files/js/demo-tour/site-tour.css', array('group' => CSS_THEME));
		drupal_add_js(drupal_get_path('theme', 'vermeer') . '/files/js/demo-tour/jquery.joyride-2.1.js', array('group' => JS_THEME, 'scope' => 'footer'));
		drupal_add_js(drupal_get_path('theme', 'vermeer') . '/files/js/demo-tour/site-tour.js', array('group' => JS_THEME, 'scope' => 'footer'));
		
	}

}


/**
 * Preprocess page variables
 */
 
 

function vermeer_preprocess_page(&$vars) {
    global $_domain;
    global $user;
 
    // Temporary arrays used to create shortcuts
    $tmp_domain_default = domain_default();
    if(strpos($_SERVER['HTTP_HOST'], 'devsr.com')) {
        $tmp_domain_default = str_replace(".com", ".devsr.com", $tmp_domain_default);
    }
    if(strpos($_SERVER['HTTP_HOST'], 'stagesr.com')) {
        $tmp_domain_default = str_replace(".com", ".stagesr.com", $tmp_domain_default);
    }
    $tmp_domain_default['default'] = 'http://' . $tmp_domain_default['subdomain'];
    $tmp_domain_default['current'] = $tmp_domain_default['default'] . '/admin/structure/domain/view/' . $_domain['domain_id'];
    
    // Detect admin-level user roles
    if(in_array('Administrator', $user->roles) || in_array('Site Manager', $user->roles)) {
        $vars['domain_info']['is_admin'] = true;
    }
    else {
        $vars['domain_info']['is_admin'] = false;
    }
    
    // Are we currently viewing the default domain?
    $vars['domain_info']['is_default'] = $_domain['is_default'];
    
    // Add domain settings for portal menu
    $vars['domain_info']['default'] = $tmp_domain_default['default'];
    $vars['domain_info']['current'] = $tmp_domain_default['current'];
    
    // Remove temporary arrays
    unset($tmp_domain_default);

}


/**
 * Preprocess function for sitemap
 */
function vermeer_preprocess_site_map(&$variables) {
    global $_domain;
    $variables['menus'] = str_replace($_domain['sitename'] . ' | ', '', $variables['menus']);
}


/* --------------------------
 * PREPARE NAVIGATION MARKUP
 * -------------------------- */

/*
 * Main Menu Function
 */
function vermeer_main_menu($main_menu) {
    $pid = variable_get('menu_main_links_source', 'main-menu');
    $full_menu_tree = menu_tree_all_data($pid);
    $full_menu_rendered = str_replace("http://local", "", vermeer_main_menu_tree_output($full_menu_tree, 10, $main_menu));
    return $full_menu_rendered;
}

/**
 * Helper function to render menu tree
 */
function vermeer_main_menu_tree_output($tree, $level = 10) {
    $output = '';
    $items = array();
    $current_path = explode('/', urldecode(request_uri()));
    
    // Pull out just the menu items we are going to render so that we
    // get an accurate count for the first/last classes.

    if(is_array($tree)) {
        foreach ($tree as $data) {
            if (!$data['link']['hidden']) {
                $items[] = $data;
            }
        }

        $items = array_slice($items, 0, 7);
        $num_items = count($items);

        $output = '<ul class="nav-list group">';
        
        foreach ($items as $i => $data) {
            // Set variables
            $mid_class = 'menu-' . $data['link']['mlid'];
            $title = i18n_string('menu:item:' . $data['link']['mlid'] . ':title', $data['link']['title']);
            $description = i18n_string('menu:item:' . $data['link']['mlid'] . ':title', $data['link']['description']);
            $link_path = $data['link']['link_path'];
            $child_elements = $data['below'];
            
            // First/Last Position Class
            
            // Get menu path
            $menu_path = explode('/', url($link_path, array('alias' => TRUE)));
            
            // Active Trail Class
            if ($menu_path[1] == $current_path[1] && $current_path[1] == '') {
                $active_class = '';
            }
            elseif (isset($menu_path[3])) {
                if ($menu_path[3] == $current_path[1]) {
                    $active_class = ' selected';
                }
                else {
                    $active_class = '';
                }
            }
            elseif ($menu_path[1] == $current_path[1]) {
                $active_class = ' selected';
            }
            else {
                $active_class = '';
            }
            
            // Truncating of Title
            if ($num_items >= 7) {
                $link_name = truncate_utf8($title, 40, TRUE, FALSE, 1);
            }
            else if ($num_items == 6) {
                $link_name = truncate_utf8($title, 55, TRUE, FALSE, 1);
            }
            else if ($num_items == 5) {
                $link_name = truncate_utf8($title, 60, TRUE, FALSE, 1);
            }
            else {
                $link_name = truncate_utf8($title, 80, TRUE, FALSE, 1);
            }
            //if the item has children then add then
            if($child_elements) {
           		$output .= '<li class="dynamic has-children ' . $active_class .  '" aria-haspopup="true">';
            } else {
            	$output .= '<li class="dynamic ' . $active_class .  '">';
            }
            //anchor 
			$output .= '<a  href="' . url($link_path) .  '" ><span class="nav-vertical-align"><span>' . $link_name . '</span></span></a>';
			//if the item has children then create a drop down
			if($child_elements) {
                $num_items_children = count($child_elements);
                $output .= '<ul>';
                //repeat of the parent item to show it on mobile - hidden on desktop view 
                $output .= '<li class="hide">';
				$output .= '<a  href="' . url($link_path) .  '" >' . $link_name . '</a>';
                $output .= '</li>';
                $index = 0;
                
                //for each of the child items 
                foreach ($child_elements as $i_children => $data_children) {
                    
                    if ($data_children['link']['hidden']) {
                        continue;
                    }

                    // Child variables
                    $title_children = i18n_string('menu:item:' . $data_children['link']['mlid'] . ':title', $data_children['link']['title']);
                    $description_children = i18n_string('menu:item:' . $data['link']['mlid'] . ':title', $data_children['link']['description']);
                    $link_path_children = $data_children['link']['link_path'];

                    
                    $output .= '<li>';
                    $output .= l($title_children, $link_path_children, array('attributes' => array('title' => $description_children), 'html' => TRUE));
                    $output .= '</li>';
                    $index++;
                }
                $output .= '</ul>';
			}
			
            $output .= '</li>';
        }
        $output .= '</ul>';
        return $output;
    }
    else {
        return null;
    }
}

/*
 * Mega Menu Function
 */
function vermeer_mega_menu($main_menu) {
    $pid = variable_get('menu_main_links_source', 'main-menu');
    $full_menu_tree = menu_tree_all_data($pid);
    $full_menu_rendered = str_replace("http://local", "", vermeer_mega_menu_tree_output($full_menu_tree, 10, $main_menu));

    return $full_menu_rendered;
}

/**
 * Helper function to render menu tree
 */
function vermeer_mega_menu_tree_output($tree, $level = 10) {
    $output = '';
    $items = array();
    
    // Pull out just the menu items we are going to render so that we
    // get an accurate count for the first/last classes.
    if(is_array($tree)) {
        foreach ($tree as $data) {
            if (!$data['link']['hidden']) {
                $items[] = $data;
            }
        }

        $num_items = count($items);
        $output = '<ul id="main-menu-links">';

        foreach ($items as $i => $data) {
        
            // Set variables
            $mid_class = 'menu-' . $data['link']['mlid'];
            $title = i18n_string('menu:item:' . $data['link']['mlid'] . ':title', $data['link']['title']);
            $link_path = $data['link']['link_path'];
            $child_elements = $data['below'];
            
            // Position Class
            if ($i == 0) {
                $position_class = ' first';
            }
            else if ($i == $num_items - 1) {
                $position_class = ' last';
            }
            else {
                $position_class = '';
            }
                
            $output .= '<li class="' . $mid_class . $position_class . '">';
            $output .= l($title, $link_path, array('attributes' => array('title' => $title), 'html' => TRUE));

            if ($child_elements) {

                $num_items_children = count($child_elements);
                $output .= '<ul class="second">';
                $index = 0;
                foreach ($child_elements as $i_children => $data_children) {
                    
                    if ($data_children['link']['hidden']) {
                        continue;
                    }

                    // Child variables
                    $title_children = i18n_string('menu:item:' . $data_children['link']['mlid'] . ':title', $data_children['link']['title']);
                    $description_children = i18n_string('menu:item:' . $data['link']['mlid'] . ':title', $data_children['link']['description']);
                    $link_path_children = $data_children['link']['link_path'];

                    if ($index == 0) {
                        $class_children = 'class="first"';
                    }
                    else if ($index == $num_items_children - 1) {
                        $class_children = 'class="last"';
                    }
                    else {
                        $class_children = '';
                    }
                    
                    $output .= '<li ' . $class_children . '>';
                    $output .= l($title_children, $link_path_children, array('attributes' => array('title' => $description_children), 'html' => TRUE));
                    $output .= '</li>';
                    $index++;
                }
                $output .= '</ul>';
                
            }
            $output .= '</li>';
        }
        $output .= '</ul>';
        return $output;
    }
    else {
        return null;
    }
}

/*
 * Main Menu Function
 */
function vermeer_footer_main_menu($main_menu) {
    $pid = variable_get('menu_main_links_source', 'main-menu');
    $full_menu_tree = menu_tree_all_data($pid);
    $full_menu_rendered = str_replace("http://local", "", vermeer_footer_menu_tree_output($full_menu_tree, 10, $main_menu));
    
    return $full_menu_rendered;
}

/**
 * Helper function to render menu tree
 */
function vermeer_footer_menu_tree_output($tree, $level = 10) {
    $output = '';
    $items = array();
    
    // Pull out just the menu items we are going to render so that we
    // get an accurate count for the first/last classes.
    if(is_array($tree)) {
        foreach ($tree as $data) {
            if (!$data['link']['hidden']) {
                $items[] = $data;
            }
        }
        
        $output = '<ul id="step-secondary-nav" class="reset content-left margin-right-4x margin-bottom-1x">';
        
        foreach ($items as $i => $data) {
            // Set variables
            $title = i18n_string('menu:item:' . $data['link']['mlid'] . ':title', $data['link']['title']);
            $link_path = $data['link']['link_path'];
            $child_elements = $data['below'];
                
            $output .= '<li>';
            $output .= l($title, $link_path, array('attributes' => array('title' => $title), 'html' => TRUE));
            $output .= '</li>';
        }
        $output .= '</ul>';
        return $output;
    }
    else {
        return null;
    }
}

/*
 * Secondary Menu Function
 */
function vermeer_secondary_menu($secondary_menu) {
    return theme('links__system_secondary_menu', array(
                'links' => $secondary_menu,
                'attributes' => array(
                    'class' => 'reset content-left margin-bottom-1x'
                    )
                ));
}

/*
 * Teritary Menu Function
 */
function vermeer_teritary_menu($_domain) {

    $domain_id = $_domain['domain_id'];
    $menuname = vermeer_var_get('menu_tertiary_links_source', array(
                    'domain_id' => $domain_id,
                    'source' => 'domain_conf'
                ));
    
    $menu = menu_navigation_links($menuname);
    $output = ' ';
	foreach($menu as $item) {
		$output .= '<a href="' . url($item['href']) . '">' . $item['title'] . '</a> | ';
	}
	$output .= '<a href="/sitemap">Site Map</a> | ';
    return $output;
}

// Side bar menu 

function vermeer_menu_tree__menu_block($variables) {
	
	$output = '';
	$output .= '<ul class="reset children-margin-bottom-half">';
	$output .= $variables['tree'];
	$output .= '</ul>';
	return $output;
}

function vermeer_menu_link__menu_block(array $variables) {
	
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  //$output = l($element['#title'], $element['#href'], $element['#localized_options']);
  
  return '<li>' . '<a href="' . url($element['#href']) . '" class="font-color-medium"><b>' . $element['#title'] . '</b></a>' .  "</li>\n";
  
}

/* --------------------------
 * GLOBAL UTILITY FUNCTIONS
 * -------------------------- */

/**
 * Variable Getter function for domain/language specific variables
 */
function vermeer_var($var_name) {
    global $_domain;
    $options['domain_id'] = $_domain['domain_id'];
    $options['i18n_string_options'] = array('format' => 'full_rte');
    return vermeer_var_get($var_name, $options);
}

/**
 * Parse out youtube id from url
 */
function vermeer_youtube_parse($url) {
    if(preg_match('%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
        return $match[1];
    }
}


/* --------------------------
 * HOME PAGE
 * -------------------------- */

/** 
 * Build Primary Marquee
 */
function vermeer_home_marquee_primary($node) {
    $html = '';
	
    if (isset($node->field_primary_marquees['und']) && count($node->field_primary_marquees['und']) > 0) {
	    $image = "";
	    $link = "";
	    $alt = "";
	    $showOverlay = true;
	    $showOverlayClass = " ";
	    //mobile carousel : Hidden on desktop 
	    $html .= '<div class="splash-image-slider-container">';
	    $html .= "<div class=\"flexslider splash-image-slider\" data-flexslider-theme=\"white\" data-flexslider-parameters='{
		\"animation\": \"slide\",
		\"animationSpeed\": 400,
		\"slideshow\": true,
		\"smoothHeight\": false,
		\"controlNav\": true,
		\"directionNav\": true,
		\"useCSS\": true,
		\"touch\": true,
		\"prevText\": \"\",
		\"nextText\": \"\",
		\"controlsContainer\": \".splash-slider-controls\",
		\"sync\": \".splash-content-slider\"
		}'>	";
		
		$html .= '<ul class="slides">';
        foreach ($node->field_primary_marquees['und'] as $index => $marquee) {
	        $showOverlay = true;
	        $showOverlay = $marquee['node']->field_show_dark_overlay['und'][0]['value'];
	        if($showOverlay) {
		        $showOverlayClass = " ";
	        } else  {
		        $showOverlayClass = "hideOverlay";
	        }
            if (isset($marquee['node']->field_marquee_image['und'][0]['uri'])) {
                $image = image_style_url('homepage_marquee', $marquee['node']->field_marquee_image['und'][0]['uri']);
            }
            if (isset($marquee['node']->field_marquee_image['und'][0]['alt'])) {
                $alt = $marquee['node']->field_marquee_image['und'][0]['alt'];
            }
            if (isset($marquee['node']->field_marquee_link_type['und'])) {
                switch ($marquee['node']->field_marquee_link_type['und'][0]['value']) {
                    case 'Path Link';
                        $link = '/' . trim($marquee['node']->field_marquee_path_link['und'][0]['safe_value'], '/');
                        break;

                    default:
                        if (isset($marquee['node']->field_marquee_link['und'][0]['nid'])) {
                            $link = url('node/' . $marquee['node']->field_marquee_link['und'][0]['nid'], $options = array('alias' => TRUE));
                        }
                        break;
                }
            }
            
            if (isset($image) && !empty($link)) {
				
                $html .= '<li>';
                $html .= '<div class="splash-image ' . $showOverlayClass . '"  data-image-large="' .  $image .'" data-image-small="' . $image . '"></div>';
                $html .= '</li>';
            }
            unset($link);
        }
        
         $html .= '</ul>';
         $html .= '</div> <!-- .flexslider -->';
         $html .= '<div class="splash-slider-controls" data-flexslider-theme="white"></div>';
         $html .= '</div>';
        
         
         
         //desktop version of the carousel
         
         $html .= "<div class=\"flexslider splash-content-slider\" data-flexslider-theme=\"white\" data-flexslider-parameters='{
		\"animation\": \"slide\",
		\"animationSpeed\": 400,
		\"slideshow\": true,
		\"smoothHeight\": false,
		\"controlNav\": false,
		\"directionNav\": false,
		\"useCSS\": true,
		\"touch\": true,
		\"prevText\": \"\",
		\"nextText\": \"\",
		\"run_function\": \"splash_slider_run\",
		\"after_function\": \"splash_slider_after\"
		}'>";
		
		//<!-- Constraint: Max of 6 slides -->

		$html .= '<ul class="slides">';
		
        foreach ($node->field_primary_marquees['und'] as $index => $marquee) {
	        $title = "";
	        $alt = "";
	        $url = "";
	        $longDescrip = "";
	        $smallDescrip = "";
	        $linklabel = "";
	        $showOverlay = true;
	        $showOverlay = $marquee['node']->field_show_dark_overlay['und'][0]['value'];
	        if($showOverlay) {
		        $showOverlayClass = " ";
	        } else  {
		        $showOverlayClass = "hideOverlay";
	        }
            if (isset($marquee['node']->field_marquee_image['und'][0]['uri'])) {
                $image = image_style_url('homepage_marquee', $marquee['node']->field_marquee_image['und'][0]['uri']);
            }
            if (isset($marquee['node']->field_marquee_image['und'][0]['alt'])) {
                $alt = $marquee['node']->field_marquee_image['und'][0]['alt'];
            }
            if (isset($marquee['node']->field_marquee_link_type['und'])) {
                switch ($marquee['node']->field_marquee_link_type['und'][0]['value']) {
                    case 'Path Link';
                        $link = '/' . trim($marquee['node']->field_marquee_path_link['und'][0]['safe_value'], '/');
                        break;

                    default:
                        if (isset($marquee['node']->field_marquee_link['und'][0]['nid'])) {
                            $link = url('node/' . $marquee['node']->field_marquee_link['und'][0]['nid'], $options = array('alias' => TRUE));
                        }
                        break;
                }
            }
            
            if(isset($marquee['node']->field_long_description['und'][0]['safe_value'])) {
	            $longDescrip = $marquee['node']->field_long_description['und'][0]['safe_value'];
            } 
            
            if(isset($marquee['node']->field_small_description['und'][0]['safe_value'])) {
	            $smallDescrip = $marquee['node']->field_small_description['und'][0]['safe_value'];
            } 
            if(isset($marquee['node']->field_link_label['und'][0]['safe_value'])) {
	            $linklabel = $marquee['node']->field_link_label['und'][0]['safe_value'];
            }
            if (isset($image) && !empty($link)) {
				
                $html .= '<li class="' . $showOverlayClass . '" data-image-large="' . $image . '" data-image-small="' . $image . '">';
                $html .= '<div class="container-inside">';
                $html .= '<div class="splash-content" style="min-height:200px">';
                //Marquee Description
                if(!empty($longDescrip)) {
                	$html .= '<h2 class="heading jumbo">' . $longDescrip . '</h2>';
                }
                //Small Marquee Description
                if(!empty($smallDescrip)) {
                	$html .= '<h3>' . $smallDescrip .  '</h3>';
                }
                //CTA link and label
                if(!empty($linklabel) && !empty($link)) {
                	$html .= '<p><a href="' . $link .  '" class="button" data-theme="yellow" data-size="large" tabindex="0">' . $linklabel . '</a></p>';
                }
                $html .= '</div> <!-- .splash-content -->';
                $html .= '</div> <!-- .container-inside -->';
                $html .= '</li>';
            }
            unset($link);
        }
        
        $html .= '</ul>';
        $html .= '</div> <!-- .flexslider -->';
		
         
    }
    // Legacy Code -- REMOVE AFTER ALL DEALERS ARE MIGRATED
    else {
        $fieldNames = array();

        // Detect fields and assign to shortcuts
        if(isset($node->field_home_marquee1_img['und'][0])) {
            $fieldNames[1]['image'] = image_style_url('homepage_marquee', $node->field_home_marquee1_img['und'][0]['uri']);
        }
        if(isset($node->field_home_marquee1_link['und'][0])) {
            $fieldNames[1]['link'] = $node->field_home_marquee1_link['und'][0]['nid'];
        }
        if(isset($node->field_home_marquee2_img['und'][0])) {
            $fieldNames[2]['image'] = image_style_url('homepage_marquee', $node->field_home_marquee2_img['und'][0]['uri']);
        }
        if(isset($node->field_home_marquee2_link['und'][0])) {
            $fieldNames[2]['link'] = $node->field_home_marquee2_link['und'][0]['nid'];
        }
        if(isset($node->field_home_marquee3_img['und'][0])) {
            $fieldNames[3]['image'] = image_style_url('homepage_marquee', $node->field_home_marquee3_img['und'][0]['uri']);
        }
        if(isset($node->field_home_marquee3_link['und'][0])) {
            $fieldNames[3]['link'] = $node->field_home_marquee3_link['und'][0]['nid'];
        }
        if(isset($node->field_home_marquee4_img['und'][0])) {
            $fieldNames[4]['image'] = image_style_url('homepage_marquee', $node->field_home_marquee4_img['und'][0]['uri']);
        }
        if(isset($node->field_home_marquee4_link['und'][0])) {
            $fieldNames[4]['link'] = $node->field_home_marquee4_link['und'][0]['nid'];
        }

        // Build final HTML markup
        foreach ($fieldNames as $index => $value) {
            if (isset($value['image']) && !empty($value['link'])) {
                $html .=  '<div class="marquee-callout callout-' . $index . '">';
                $html .= '<a href="' . (isset($value['link']) ? url('node/' . $value['link'], $options = array('alias' => TRUE)) : '') . '"><img src="' . $value['image'] . '" /></a>';
                $html .= '</div>';
            }

        }
    }

    return $html;
}

/** 
* This is no longer part of the new redesign 
 * Build Secondary Marquee
 */
function vermeer_home_marquee_secondary($node) {
    $html = '';
    
    // Image
    $image = field_get_items('node', $node, 'field_home_marquee_sec_img');
    $image = field_view_value('node', $node, 'field_home_marquee_sec_img', $image[0]);
    
    // Title
    $title = field_get_items('node', $node, 'field_home_marquee_sec_title');
    $title = field_view_value('node', $node, 'field_home_marquee_sec_title', $title[0]);
    
    // Description
    $description = field_get_items('node', $node, 'field_home_marquee_sec_desc');
    $description = field_view_value('node', $node, 'field_home_marquee_sec_desc', $description[0]);
    
    // Button Text
    $button_text = field_get_items('node', $node, 'field_home_marquee_sec_button');
    $button_text = field_view_value('node', $node, 'field_home_marquee_sec_button', $button_text[0]);
    
    // Button Path
    $button_path = field_get_items('node', $node, 'field_home_marquee_sec_link');
    $button_path = $button_path[0]['nid'];

    // Build final HTML markup
    if($title) {
        $html .= render($image);
        $html .= '<h2>' . render($title) . '</h2>';
        if ($description) {
            $html .= '<p>' . render($description) . '</p>';
        }
        if ($button_text) {
            $html .= '<p class="yellow-button"><a href="' . url('node/' . $button_path, $options = array('alias' => TRUE)) . '">' . render($button_text) . '</a></p>';
        }
    }    
    
    return $html;
}

// Build Home Footer Parallax CTA
function footerparallaxcta($node) {
	//the html to return and display
    $html = '';
    $fields = array();
    
    //Cta title
    $ctaOn = field_get_items('node', $node, 'field_display_footer_call_to_act');
    $fields['ctaOn'] = field_view_value('node', $node, 'field_footer_parallax_title', $ctaOn[0]);
    $cta_title = field_get_items('node', $node, 'field_footer_parallax_title');
    $fields['title'] = field_view_value('node', $node, 'field_footer_parallax_title', $cta_title[0]);
	//CTA Description 
    $cta_desc = field_get_items('node', $node, 'field_footer_parallax_desc');
    $fields['description'] = field_view_value('node', $node, 'field_footer_parallax_desc', $cta_desc[0]);
	//CTA Link
    $cta_link = field_get_items('node', $node, 'field_footer_parallax_node_link');
    $fields['link'] = field_view_value('node', $node, 'field_footer_parallax_node_link', $cta_link[0]);
    //CTA Link label
    $cta_link_label = field_get_items('node', $node, 'field_footer_parallax_link_label');
    $fields['link_label'] = field_view_value('node', $node, 'field_footer_parallax_link_label', $cta_link_label[0]);
    //CTA Image
    $cta_image = field_get_items('node', $node, 'field_footer_parallax_image');
    $fields['image'] = field_view_value('node', $node, 'field_footer_parallax_image', $cta_image[0]);
    
	//if the array is populated then build the html and if its on
	if($fields['ctaOn']['#markup']) {
		if(!empty($fields)) {
			$title = $fields['title']['#markup'];
			$description = $fields['description']['#markup'];
			$link = url($fields['link']['#href']);
			$label = $fields['link_label']['#markup'];
			$image_src = file_create_url($fields['image']['#item']['uri']);
			
			$html .= '<section class="content-span content-span-fixed" data-image-large="'. $image_src . '" data-image-small="' . $image_src .'" data-cs-theme="white"><!-- footer parallax cta -->';
	    	$html .= '<div class="content-span-fixed-inner">';
	    	$html .= '<div  class="container-inside align-center font-color-white shadow-black">';
	    	$html .= '<h3 id="step-footer-cta" class="heading jumbo margin-top-none">' . $title . '</h3>';
	    	$html .= '<p  class="margin-bottom-none">' . $description . '<br><a href="' . $link . '" class="bright">' . $label . ' &raquo;</a></p>';
	    	$html .= '</div> <!-- .container-inside -->';
	    	$html .= '</div> <!-- .content-span-fixed-inner -->';
	    	$html .= '</section> <!-- end footer parallax cta -->';
		}
	}
    return $html;
    
}


/** 
 * Build Resource Callouts
 */
function vermeer_home_resources($node) {
    $html = '';
    $fields = array();

    // Title 1
    $field_title1 = field_get_items('node', $node, 'field_home_resource1_title');
    $fields[1]['title'] = field_view_value('node', $node, 'field_home_resource1_title', $field_title1[0]);
    // Description 1
    $field_desc1 = field_get_items('node', $node, 'field_home_resource1_desc');
    $fields[1]['description'] = field_view_value('node', $node, 'field_home_resource1_desc', $field_desc1[0]);
    // Image 1
    $field_image1 = field_get_items('node', $node, 'field_home_resource1_img');
    $fields[1]['image'] = field_view_value('node', $node, 'field_home_resource1_img', $field_image1[0]);
    // Link 1
    $fields[1]['link'] = field_view_field('node', $node, 'field_home_resource1_link', array('label' => 'hidden'));
    
    // Title 2
    $field_title2 = field_get_items('node', $node, 'field_home_resource2_title');
    $fields[2]['title'] = field_view_value('node', $node, 'field_home_resource2_title', $field_title2[0]);
    // Description 2
    $field_desc2 = field_get_items('node', $node, 'field_home_resource2_desc');
    $fields[2]['description'] = field_view_value('node', $node, 'field_home_resource2_desc', $field_desc2[0]);
    // Image 2
    $field_image2 = field_get_items('node', $node, 'field_home_resource2_img');
    $fields[2]['image'] = field_view_value('node', $node, 'field_home_resource2_img', $field_image2[0]);
    // Link 3
    $fields[2]['link'] = field_view_field('node', $node, 'field_home_resource2_link', array('label' => 'hidden'));
    
    // Title 3
    $field_title3 = field_get_items('node', $node, 'field_home_resource3_title');
    $fields[3]['title'] = field_view_value('node', $node, 'field_home_resource3_title', $field_title3[0]);
    // Description 3
    $field_desc3 = field_get_items('node', $node, 'field_home_resource3_desc');
    $fields[3]['description'] = field_view_value('node', $node, 'field_home_resource3_desc', $field_desc3[0]);
    // Image 3
    $field_image3 = field_get_items('node', $node, 'field_home_resource3_img');
    $fields[3]['image'] = field_view_value('node', $node, 'field_home_resource3_img', $field_image3[0]);
    // Link 3
    $fields[3]['link'] = field_view_field('node', $node, 'field_home_resource3_link', array('label' => 'hidden'));
    
    $html .= '<section class="content-span" data-cs-padding="2 0" data-cs-theme="white">';
    $html .= '<div class="container-inside">';
    $html .= '<ul id="slide-callouts" class="columns columns-3" data-bp="mobilelandscape">';
    // Build the final HTML markup
    foreach ($fields as $index => $field) {
        if ($field['title'] && $field['description']) {
	        $html .= '<!-- resource callout #' . $index . ' --> ';
	        //add  class="column-row" to first item 
	        if($index == 1) {
	        	$html .= '<li class="column-row" data-margin-bottom-2x-bp="mobilelandscape">';
	        } else if($index == 2) {
	        	$html .= '<li data-margin-bottom-2x-bp="mobilelandscape">';
	        } else {
	        	$html .= '<li>';
	        }
            $html .= '<div class="bg-to-round margin-bottom-1x" style="background-image:';
            //if it has bg image then...
            if (count($field['image']['#item']) > 0) {
            	$html .= 'url(' . file_create_url($field['image']['#item']['uri']) . ');"'; 
                //$html .= '<pre>' . echo (file_create_url($field['image']['#item']['uri'])) . '</pre>';
                
            } else { //else just add a placeholder image
            	$html .= 'url(http://placehold.it/295x112);"'; 
            }
            $html .= ' data-content-left-bp="mobilelandscape" data-margin-right-bp="mobilelandscape"></div>';
            $html .= '<div class="overflow">';
            $html .=  '<h4 class="heading margin-vertical-none">' . render($field['title']) . '</h4>';
            $html .= '<p class="margin-bottom-none">' . render($field['description']); 
            if ($field['link']) {
                $html .= ' <a class="no-wrap" href="' . url('node/' . $field['link']['#items'][0]['nid'], $options = array('alias' => TRUE)) . '">' . t('Read more') . '</a>';
            }
            $html .= '</p>';
            $html .= '</div>';
            $html .= '</li>';
	        $html .= '<!-- .resource callout #' . $index . ' --> ';
        }
    }
    $html .= '</ul> <!-- .resource callouts --> ';
    $html .= '</div> <!-- .container-inside -->';
    $html .= '</section> <!-- .content-span -->';
    
    return $html;
}

// Pager OUTPUT
function vermeer_pager_next($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array, $pager_total;

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The next page is not the last page.
    else {
     	$output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  }

  return $output;
}



// Function 

function vermeer_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574
  $attributes['href'] = url($_GET['q'], array('query' => $query));
  return '<a data-theme="yellow" data-size="large" class="button"' . drupal_attributes($attributes) . '>' . check_plain($text) . '</a>';
}


/** 
 * Add Current Page Title it Breadcrumbs
 */
 
function vermeer_breadcrumb($variables) {
    $is_front = drupal_is_front_page();

    if (!isset($variables['breadcrumb'][0]) || $variables['breadcrumb'][0] == '') {
        return;
    }
    
    if (!$is_front && count($variables['breadcrumb']) > 0) {    
	    
	     $lastitem = sizeof($variables['breadcrumb']);
	     $title = drupal_get_title();
	     $crumbs = '<h3 class="heading lined margin-top-none">';
	     $a=1;
	     foreach($variables['breadcrumb'] as $value) {
	     	$crumbs .= $value . ' &raquo; ';
	     	$a++;
	     }
	     $crumbs .= '<span>' . drupal_get_title() . '</span>';
	     $crumbs .= '</h3>';
		 	return $crumbs;
	   	} 
        // Add Page Title
        
        //$variables['breadcrumb'][] = ;
   
    //return theme_breadcrumb($variables);
}


//modify form element
//https://api.drupal.org/api/drupal/includes%21form.inc/function/theme_form_element/7
function vermeer_form_element($variables) {
  $element = &$variables['element'];
  $output = '';
  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  //form element wrapper

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;
	  
    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }
  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }


  return $output;
}
//Custom form edits
function vermeer_form_alter(&$form, &$form_state, $form_id) {
	//print_r($form);
	//add the recaptcha only to forms created by dealers
	if(preg_match("/^webform_client_form_[0-9]+$/",$form_id) && user_is_logged_in() == FALSE && false) {
		
	    $form['my_captcha_element'] = array(
	      '#type' => 'captcha',
          '#captcha_type' => 'recaptcha/reCAPTCHA',
	    );
		
	}
	//sales rep search bar on the find a sales rep page
	if($form['#id'] == "views-exposed-form-find-sales-rep-page-1") {
		$form['zipcode']['#attributes']['class'][] = 'match-height content-left'; 
		$form['zipcode']['#attributes']['data-size'][] = 'medium'; 
		$form['submit']['#attributes']['class'][] = 'button match-height content-left'; 
		$form['submit']['#attributes']['data-theme'][] = 'yellow'; 
		$form['submit']['#attributes']['data-size'][] = 'small'; 
	}
	//to change the URL to search sales rep from used equipment page
	if($form['#id'] == "views-exposed-form-find-sales-rep-page-3") {
		$form['#action'] = url('/find-sales-rep');
		$form['submit']['#attributes']['class'][] = 'button match-height content-left'; 
		$form['submit']['#attributes']['data-theme'][] = 'yellow'; 
		$form['submit']['#attributes']['data-size'][] = 'small'; 
		$form['zipcode']['#attributes']['class'][] = 'match-height content-left'; 
		$form['zipcode']['#attributes']['data-size'][] = 'medium'; 
	}
	
	//add attributes to the used equipment form elements
	if($form['#id'] == "views-exposed-form-used-equipment-block") {
		//add class to these fields with the name of ...
		$form['keywords']['#attributes']['class'][] = 'full-width'; 
		$form['make']['#attributes']['class'][] = 'full-width'; 
		$form['type']['#attributes']['class'][] = 'full-width'; 
		$form['sort_by']['#attributes']['class'][] = 'full-width'; 
		//$form['sort_order']['#attributes']['class'][] = 'hide'; 
		$form['submit']['#attributes']['class'][] = 'full-width button'; 
		$form['submit']['#attributes']['data-theme'][] = 'yellow'; 
		$form['submit']['#attributes']['data-size'][] = 'large'; 
		$form['reset']['#attributes']['class'][] = 'reset font-color-medium';
	}
}

function vermeer_menu_local_tasks(&$variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] .= '<ul>';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] .= '<ul>';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}



function vermeer_date_nav_title($params) {
    $granularity = $params['granularity'];
    $view = $params['view'];
    $date_info = $view->date_info;
    $link = !empty($params['link']) ? $params['link'] : FALSE;
    $format = !empty($params['format']) ? $params['format'] : NULL;
    switch ($granularity) {
        case 'year':
            $title = $date_info->year;
            $date_arg = $date_info->year;
            break;
        case 'month':
            $format = !empty($format) ? $format : (empty($date_info->mini) ? 'F Y' : 'F');
            $title = date_format_date($date_info->min_date, 'custom', $format);
            $date_arg = $date_info->year . '-' . date_pad($date_info->month);
            break;
        case 'day':
            $format = !empty($format) ? $format : (empty($date_info->mini) ? 'l, F j, Y' : 'l, F j');
            $title = date_format_date($date_info->min_date, 'custom', $format);
            $date_arg = $date_info->year . '-' . date_pad($date_info->month) . '-' . date_pad($date_info->day);
            break;
        case 'week':
            $format = !empty($format) ? $format : (empty($date_info->mini) ? 'F j, Y' : 'F j');
            $title = t('Week of @date', array('@date' => date_format_date($date_info->min_date, 'custom', $format)));
            $date_arg = $date_info->year . '-W' . date_pad($date_info->week);
            break;
    }
    if (!empty($date_info->mini) || $link) {
        // Month navigation titles are used as links in the mini view.
        $attributes = array('title' => t('View full page month'));
        $url = date_pager_url($view, $granularity, $date_arg, TRUE);
        return l($title, 'events', array('attributes' => $attributes, 'query' => array('date' => $date_arg, 'mini' => (isset($_GET['mini']) ? $_GET['mini'] : ''))));
    }
    else {
        return $title;
    }
    
    //function to output PHP to the console
}
