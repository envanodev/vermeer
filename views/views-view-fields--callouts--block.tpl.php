<?php
	//Variables
	//$field - Contains an array of each result, in this case the callout itself
	//$type - the type of callout, text or image
	//$calloutURL - the internal URL of the callout 
	//$imageURL - the image path if its an image type callout 
	//
	//
    // Set shortcut variable for each field
    $field = $row->_field_data['nid']['entity'];
    $type = (isset($field->field_callout_type['und'][0]['value']) ? $field->field_callout_type['und'][0]['value'] : '');
    $calloutTitle = $field->title;
    //print_r($field);
    
    
    
    if($type == 'image') {
	    $imageURL = image_style_url('callout_image', $field->field_callout_image['und']['0']['uri']);
    } 
    
    if($type == 'text') {
	    $calloutDescpt = $field->field_callout_description['und'][0]['safe_value'];
	    $calloutLinkLabel = $field->field_callout_link_text['und'][0]['safe_value'];
	    
    }
    
    // Get callout nid and use nid to get alias
    if (isset($field->field_callout_url['und'][0]['nid'])) {
        $callout_reference_nid = $field->field_callout_url['und'][0]['nid'];
        $calloutURL = url('node/' . $callout_reference_nid);
    }
    else {
        $callout_reference_nid = NULL;
        $calloutURL = NULL;
    }
    
    
    
    // Get current nid
    $current_nid = (arg(0) == 'node' && is_numeric(arg(1))) ? arg(1) : FALSE;
    if ($current_nid) {
        $translation_set = vermeer_var_get_translation_set(node_load($current_nid));
    }
    else {
        $translation_set = array();
    }
  $counter = 0;  
?>

<?php if (!in_array($callout_reference_nid, $translation_set)) { ?>
        <?php foreach ($fields as $id => $field) {?>
	        	
            <?php 
	            $counter++;
	            if($counter == 1) {
		            //print $field->content; 
		            if($type == 'image') { ?>
		            	<a href="<?php echo($calloutURL) ?>">
		            	<img src="<?php echo($imageURL); ?>" data-full-width-bp="mobilelandscape" alt="<?php echo($calloutTitle);  ?>">
		            	</a>
		            <?php } elseif($type == 'text') { ?>
		            	<h3 class="margin-bottom-none"><?php echo($calloutTitle); ?></h3>
		            	<p><?php echo($calloutDescpt); ?></p>
						<a href="<?php echo($calloutURL) ?>" class="button" data-theme="yellow" data-size="small" tabindex="0"><?php echo($calloutLinkLabel); ?></a>
		            <?php } ?>
	            <?php } ?>
	            
        <?php } ?>
<?php } ?>

