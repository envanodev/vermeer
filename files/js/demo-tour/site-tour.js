      
$(window).load(function() {
   $('[id="start-tour"]').click(function(){
	   $(window).joyride('restart');
	   
   });
  $('#siteTour').joyride({
    autoStart : false,
    cookieDomain : 'demo.new.vermeerdealersystem.com',
    preRideCallback: function() {
		  $('body[data-utility-bar="true"] .container').css({
			  'padding-top': '0px',
		  });
		  
		  $('[class="utility-bar group promote-layer"]').css({
		       'position': 'relative',
		  });
		    
		$('[class="utility-bar group promote-layer"] .container-inside').css({
		       'overflow': 'hidden',
		  }); 
    
    },
    postRideCallback: function() {
		  $('body[data-utility-bar="true"] .container').css({
			  'padding-top': '32px',
		  });
		  
		  $('[class="utility-bar group promote-layer"]').css({
		       'position': 'fixed',
		  });
		    
		$('[class="utility-bar group promote-layer"] .container-inside').css({
		       'overflow': 'inherit',
		  }); 
    },
    postStepCallback : function (index, tip) {
    if (index == 1) {
      
    }
  },
  
  modal:true,
  expose: true,
  });
});
