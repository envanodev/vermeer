<?php 
	global $_domain;
	$utility_bar = vermeer_var('utility_bar');
	$analytics_id = vermeer_var('analytics_id');
    $analytics_enabled = vermeer_var('analytics_enabled');

?>
<!DOCTYPE html> 
<html lang="<?php echo $language->language; ?>" class="no-js no-document-ready no-window-load nav-closed">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" sizes="196x196" href="files/ico/196x196.png">
	<link rel="apple-touch-icon" href="files/ico/60x60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="files/ico/76x76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="files/ico/120x120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="files/ico/152x152.png">
	<link rel="shortcut icon" type="image/ico" href="files/ico/favicon.ico">
	<?php echo $head ?>
<!-- 	stylesheet -->
	<?php echo $styles ?>
<!-- 	end stylesheet -->
	<!-- javascript -->
	<!--[if lte IE 8]>
		<script src="/sites/all/themes/vermeer/files/js/html5.js"></script>
	<![endif]-->
	<?php echo $scripts ?>
<!-- end javascript -->
	<style type="text/css" media="screen">
		.content-span .ajax-progress-throbber {
		    margin: .5em auto;
		    width: 16px;
		    background-image: url('/_ui/skin/img/misc/ajax-loader.gif'); /* LTR */
		    height: 11px;
		    display: block;
		}
		.content-span .ajax-progress .throbber {
		    background: none !important;
		}
		.content-span .item-list ul li, .content-span .item-list ul li {
			padding: 0;
			margin: 0;
		}
		#views-exposed-form-find-sales-rep-page-1  .ajax-progress-throbber {
			float: left;
			margin-left: 1em;
		}
		.content-span .view .progress-disabled {
		  float: none;
		}
		.admin-tabs ul {
			overflow: hidden;
			padding: 0;
			margin: 0;
			border-bottom: solid gray 1px;
			margin: 1.4em 0em;
		}
		.admin-tabs ul li {
			float: left;
			list-style: none;
			text-align: center;
			padding: 0;
			margin: 0;
			margin-right: 2.5em;
		}
		.admin-tabs ul li a {
			display: block;
			background: rgba(223, 223, 223, 1);
			color: #1c1c1c;
			width: 100%;
			padding: .25em .75em;
			border: solid gray 1px;
			border-bottom-width: 0;
			
		}
		.admin-tabs ul li a:hover {
			background: rgba(164, 164, 164, 1);
		}
		.captcha.form-wrapper {
			border: none;
		}
		.captcha.form-wrapper legend {
			display: none;
		}
		.captcha.form-wrapper .fieldset-description {
			display: none;
		}
		.splash .slides>li.hideOverlay:after {
			opacity: 0;
		}
		
		.node-type-used-equipment-overview .view-empty .columns-3 > li  {
			float: none;
			width: 100%;
		}
		.node-type-used-equipment-overview .view-empty .columns-3 > li .equipment-box {
			padding: .5em;
			padding-left: 0;
			border: none;
			border-top: solid 1px black;
		}
		.node-type-industry .field-name-field-headline {
			display: none;
		}
		.node-type-industry .node-industry img {
			width: 100%;
			height: auto;
		}
		.footer-text a, .footer-text {
			color: #595959;
		}
	</style>
<!-- Add to script file!	 -->
 		<script>
		$(window).load(function(){
			$("[data-track-click]").click(function(){
				var link = this;
				var action = "Click";
				var tracker = $(link).data('trackClick')[0];
				//console.dir(tracker);
				ga('GlobalTracking.send', 'event', {eventCategory: tracker.category , eventAction: 'Click', eventLabel: tracker.label});
				<?php if($analytics_enabled && !empty($analytics_id)) { ?>
					ga('send', 'event', {eventCategory: tracker.category , eventAction: 'Click', eventLabel: tracker.label});
				<?php } ?>
			})
		});
	</script>

	<title><?php echo $head_title ?></title>
</head>
<body  id="specificity" data-utility-bar="<?php if($utility_bar && !user_is_logged_in()) { echo('true'); } else { echo('false'); } ?>" class="<?php echo $classes; ?>" <?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
