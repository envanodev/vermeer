 <?php
    // Get UE Hot listing NID if set
    if (isset($row->node_field_data_field_hot_listing_nid)) {
        $nid = 'node/' . $row->node_field_data_field_hot_listing_nid;
    }
    // Get UE search/UE carousel NID if set
    else {
        $nid = 'node/' . $row->nid;
    }
    $node_path = '/' . vermeer_var_lookup_path('alias', $nid);
    if($row->field_field_used_equipment_images[0]) {
    	$image_path = image_style_url('used_equipment_carousel',$row->field_field_used_equipment_images[0]['rendered']['#item']['uri'] );
    } else {
	    $image_path = "/sites/all/themes/vermeer/files/images/template/fallbacks/no-image.gif";
    }
    $alttext =  $row->field_field_used_equipment_images[0]['rendered']['#item']['alt'];
    if(empty($alttext) && !empty($row->node_title)) {
	    $alttext = $row->node_title;
    }
    $titletext = $row->field_field_used_equipment_images[0]['rendered']['#item']['title'];
    if(empty($titletext) && !empty($row->node_title)) {
	    $titletext = $row->node_title;
    }
	//print_r($row);
	
?>

<?php 
	//output the link and the image
	 print('<a href="' . $node_path . '" data-hover-effect="black"><img class="test full-width" src="' . $image_path . '" alt="' . $alttext . '" title="' . $titletext . '" /></a>');
	 //print $output; 
?>
 