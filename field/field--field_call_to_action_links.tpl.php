<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
 	$counter = 0;
?>
  <?php $links = vermeer_used_equipment_build_call_to_actions(); ?>
  	<p>
  <?php foreach ($links as $link){
		$counter ++;
	?>
	<?php if($counter == 1) { ?>
    <a class="button margin-right-half margin-bottom-half call-to-action" href="/<?php echo $link['link']; ?>" data-theme="yellow" data-size="large" tabindex="0">
        <?php echo $link['title']; ?>
    </a>
    <?php } elseif($counter == 2) { ?>
    <a class="button margin-bottom-half call-to-action" href="/<?php echo $link['link']; ?>" data-theme="light-gray" data-size="large" tabindex="0">
        <?php echo $link['title']; ?>
    </a>
  <?php } elseif($counter == 3) { ?>
  	</p>
  	<p>
    <a class="button margin-right-half margin-bottom-half margin-bottom-half call-to-action" href="/<?php echo $link['link']; ?>" data-theme="yellow" data-size="large" tabindex="0">
        <?php echo $link['title']; ?>
    </a>
  <?php		  
	  	}
	}
  ?>
  </p>
  <h3><a href="/sites/all/themes/vermeer/files/includes/server/share-equip.php?url=<?php echo urlencode(($_SERVER['HTTP_HOST'] . request_uri())); ?>&title=<?php echo(urlencode(drupal_get_title())); ?>" rel="shadowbox;height=450;width=500" class="icon-email before unbold"><span class="padding-1x left">Share This Equipment</span></a></h3>