	<?php 
		//this template is for the home page
		
	?>
	<section id="step-home-marquee" class="splash">
				
            <?php 
	            
	             echo(vermeer_home_marquee_primary($node)); 
	             
	         ?>
	
	</section
	<section class="content-span"  data-cs-theme="white">
    <?php if ($tabs['#primary']) { ?>
    	<div class="container-inside">
        	<div class="admin-tabs"><?php print render($tabs); ?></div>
    	</div>
    <?php } ?>
    <?php if (arg(2) == 'revisions') {?>
    	<div class="container-inside">
        <?php $block = module_invoke('vermeer_blocks', 'block_view', 'revision_tabs'); echo '<div class="tabs">' . $block['content'] . '</div>'; ?>
    	</div>
    <?php } ?>
    <?php if ($messages) { ?>
    	<div class="container-inside">
        <?php echo $messages; ?>
    	</div>
    <?php } ?>
    </section>

