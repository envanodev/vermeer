<?php
	$current_path = explode('/', urldecode(request_uri()));
	if(!empty($current_path[1])) {
		$current_path = $current_path[1];
	} else {
		$current_path = '';
	}
    // Get Referer URL
    $back_to_search = FALSE;
    //share equipment Link 
    $shareEquipment = FALSE;
    $referer = $_SERVER['HTTP_REFERER'];
    
    // If Referer URL is Used Equipment Search
    if (strpos(request_path(), 'used-equipment/') !== false && request_path() != 'used-equipment' && preg_match('/used-equipment([?#](.*))?/', $referer)) {
        
        // Create Referer URL
        $referer_url = explode('.com', $referer);
        $referer_url = $referer_url[1];
        
        // Back to Search is Visible
        //$back_to_search = TRUE;
        $shareEquipment = TRUE;
    }
?>
<section class="content-span" data-cs-padding="2 0" data-cs-theme="white">
	<div class="container-inside">
		<div class="main-columns fluid-fixed" data-bp="phablet">
			<div class="fluid-wrapper">
				<div class="fluid overflow">
				<?php if ($tabs && (!isset($node) || (isset($node) && ($node->type != 'special_offers' || !$node->domain_site)))) { ?>
				    <div class="admin-tabs"><?php print render($tabs); ?></div>
				<?php } ?>
				<?php print $breadcrumb?>
				<?php if ($messages) { ?>
				    <?php print $messages; ?>
				<?php } ?>

		        <?php if ($title && $_SERVER['QUERY_STRING'] == 'resource=newsrelease' || ($_SERVER['QUERY_STRING'] == 'resource=jobstory') || ($_SERVER['QUERY_STRING'] == 'resource=inthenews')) { ?>
		        <?php } elseif ($title) { ?>
		            <?php if ($back_to_search == TRUE) { ?>
		                <p class="back-to-search"><a href="<?php echo $referer_url; ?>">&#171; <?php print t('Back to Search Results'); ?></a></p>
		            <?php } ?>
		        <?php } ?>
		        <?php print render($page['content']); ?>
        		</div> <!-- .fluid-wrapper -->
        	</div> <!-- .fluid overflow -->
			<?php if ($page['sidebar_first']): ?>
			        <?php print render($page['sidebar_first']); ?>
			<?php endif; ?>

	    <?php if ($page['sidebar_second']) { ?>
	            <?php print render($page['sidebar_second']); ?>
	    <?php } elseif(arg(0) == 'new-equipment') { ?>
			<aside class="fixed ">
				<?php if($current_path == 'new-equipment') { ?>
				<h3 class="heading lined margin-top-none">Industries</h3>
				<?php } else { ?>
				<h3 class="heading lined margin-top-none"><?php print(drupal_get_title()); ?></h3>
				<?php } ?>
				<?php
					$view = views_get_view('callouts');
					print $view->preview('block_1');
				?>
			</aside>
	    <?php } ?>  
		</div> <!-- .main-columns -->
	</div> <!-- .container-inside -->
</section> <!-- .content-span -->
