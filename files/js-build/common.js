if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };
var testing = false;

$(window).load(function() {

	$("html").addClass("window-load").removeClass("no-window-load");
	
});


$(document).ready(function(){

$("html").addClass("document-ready").removeClass("no-document-ready");

if(testing) $("html").addClass("testing");


/****************************************************************
	Responsive
****************************************************************/
var canvasflexible = new responsive({
	classname: "canvasflexible",
	maxWidth: 940
});
canvasflexible.init();
canvasflexible.resize();


/****************************************************************
	jQuery plugin calls
****************************************************************/
$(".content").fitVids();

FastClick.attach(document.body);

$("img.unveil:not(.loaded)").unveil(200, function() {
	$(this).load(function() {
		$(this).addClass("loaded");
	});
});


/****************************************************************
	Equal Height
****************************************************************/
$(".equal-columns > li").setMinHeight(0).addClass("equalheight");

equal_height("[data-equal-height]",false,false);


/****************************************************************
	Touch nav hide/show
****************************************************************/
$(document).on("click nav-view", ".event-nav-view", function(e){	
	if($("html").hasClass("nav-open"))
	{
		$("html").addClass("nav-closed").removeClass("nav-open");
	} else {
		$("html").addClass("nav-open").removeClass("nav-closed");
	}
	
	//Causes the nav to redraw. Needed for stock Android browser. Otherwise nav's rendering gets cut off at 100% of the viewport :(	
	if($("#redraw").length == 0)
	{
		setTimeout(function(){
			$(".nav-mobile .nav-list").append("<li id='redraw'>&nbsp;</li>");		
		}, 1500); //Magic number, should be greater than the time it takes the nav to animate.	
	}
	
	return false;
});


/****************************************************************
	Click event for parent nav items in mobile nav
****************************************************************/
$(document).on("click", ".nav-mobile .nav-list > li.has-children > a", function(e) {
	var t = $(this);
	var parent = t.closest("li");
	parent.toggleClass("mobile-selected");
	if(typeof _gaq != "undefined") _gaq.push(['_trackEvent', 'nav', 'click', 'children', 1]);
	return false;
});


/****************************************************************
	Responsive tabs
****************************************************************/
$(document).on("click", ".tabs > li > a, .accordion > dt > a", function(e) {
	var t = $(this);
	var root = t.closest(".accordion-group");
	var href = t.attr("href");
	var tab = root.find("> .tabs > li > a[href='"+href+"']").closest("li");
	var accordion = root.find("> .accordion > dt > a[href='"+href+"']").closest("dt");

	var caller = false;
	if(t.parent().is("dt")) caller = "accordion";
	if(t.parent().is("li")) caller = "tab";

	if(typeof _gaq != "undefined") _gaq.push(['_trackEvent', 'accordion', 'click', href, 1]);

	if((root.hasClass("multiple") && caller == "accordion"))
	{
		tab.toggleClass("selected");
		accordion.toggleClass("selected");

		accordion.next("dd").slideToggle(200, function(){
			var t = $(this);
			t.toggleClass("selected");
			t.removeAttr("style");
		});
	} else {
		if(!tab.hasClass("selected") || !accordion.hasClass("selected"))
		{
			/* View one at a time - nth = root.find(".accordion > dt").index( t.closest("dt") ); */
			root.find("> .tabs > li").removeClass("selected");
			root.find("> .accordion > dt").removeClass("selected");
			root.find("> .accordion > dd").removeClass("selected");

			tab.addClass("selected");
			accordion.addClass("selected");
			
			accordion.next("dd").addClass("selected");
		} //!hasClass("selected")
	}

	return false;
});


/****************************************************************
	Generic panel open/close toggle - Primarily used for hiding/showing two elements
	Shorthand for .event-toggle-class
****************************************************************/
$(document).on("click", ".event-panel-toggle", function(e){
	var t = $(this);	
	var targetOpen = t.attr("data-panel-open-target");
	var targetClose = t.attr("data-panel-close-target");
	if(targetOpen) $(targetOpen).removeClass("hidden").addClass("visible");
	if(targetClose) $(targetClose).removeClass("visible").addClass("hidden");
	return false;
});


/****************************************************************
	Generic panel toggle menu - Primarily used for dropdown menus
****************************************************************/
$(document).on("click event-panel-toggle-menu", ".event-panel-toggle-menu", function(e){
	var t = $(this);	
	var target = t.attr("data-panel-toggle-target");
	if($(target).hasClass("hidden"))
	{
		t.addClass("active");
		$(target).addClass("visible").removeClass("hidden");
	} else {
		t.removeClass("active");
		$(target).addClass("hidden").removeClass("visible");
	}
	
	if(!t.is("[type=checkbox]"))
	{
		return false;
	}
});

//Close 'Generic panel toggle' - http://stackoverflow.com/questions/152975/how-to-detect-a-click-outside-an-element
$(document).on("click", function(e){
	var selector = ".visible[data-offclick-close=true]:visible";	
	if($(e.target).closest(selector).length == 0)
	{
		$(".event-panel-toggle-menu + " + selector).each(function(index, element) {		
			var t = $(this);
			t.prev(".event-panel-toggle-menu").trigger("event-panel-toggle-menu");
		});	
	}	
});

/****************************************************************
	Filtering
****************************************************************/
$(document).on("click", ".event-select-filter[data-filter-target]", function(e){
	var t = $(this);
	var target = $(t.attr("data-filter-target"))
	$(target).text(t.text());	
	t.closest(".filter").find(".event-panel-toggle-menu").trigger("event-panel-toggle-menu");	
	return false;
});


/****************************************************************
	Generic class toggle
****************************************************************/
$(document).on("click", ".event-toggle-class[data-toggle-class-add-target][data-toggle-class-remove-target][data-toggle-class-add][data-toggle-class-remove]", function(e){
	var t = $(this);
	var targetAdd = t.attr("data-toggle-class-add-target");
	var targetRemove = t.attr("data-toggle-class-remove-target");
	var targetAddClass = t.attr("data-toggle-class-add");
	var targetRemoveClass = t.attr("data-toggle-class-remove");
	$(targetAdd).addClass(targetAddClass);
	$(targetRemove).removeClass(targetRemoveClass);
	return false;
});


/****************************************************************
	Generic copy DOM Nodes
****************************************************************/
$("[data-copy-node-source]").each(function(index, element) {
	var t = $(this);
	var source = t.attr("data-copy-node-source");
	var cloned = $(source).clone();
	t.prepend(cloned);
});


/****************************************************************
	Dynamically load Javascript files only if needed
***************************************************************/
jQuery.cachedScript = function( url, options ) {
	options = $.extend( options || {}, {
		dataType: "script",
		cache: true,
		url: url
	});
	return jQuery.ajax( options );
};

if($(".no-overflowscrolling.touch").length > 0)
{
	$.cachedScript("/files/js/overthrow.min.js");
}


/****************************************************************
	Generic scroll to - //http://djpate.com/2009/10/07/animated-scroll-to-anchorid-function-with-jquery/
****************************************************************/
$(document).on("click", ".event-scroll-to[data-scroll-to]", function(e) {
	var t = $(this);
	var target = t.attr("data-scroll-to");
	$("html,body").animate({scrollTop: $(target).offset().top - 10},300);
	return false;
});


/****************************************************************
	.buttons
****************************************************************/
$(document).on("click", ".event-active", function(e) {
	var t = $(this);
	t.addClass("active");
});


/****************************************************************
	Opening external links in new window
****************************************************************/
$(document).on("click", "a[href*='http']:not(.new-window-ignore):not(a[href*='"+window.location.host+"'])", function(e){
	var t = $(this);
	if(typeof _gaq != "undefined") _gaq.push(['_trackEvent', 'link', 'click', t.attr("href"), 1]);
	window.open(t.attr("href"));
	return false;
});


/****************************************************************
	One off for used equipment gallery
****************************************************************/
$(document).on("click", ".event-view-all-photos", function(e) {
	var t = $(this);
	t.parent().hide();	
	$(".equipment-gallery li[data-hide-breakpoint]").removeAttr("data-hide-breakpoint");
});


/****************************************************************
	Add classes for OS & Old IE
****************************************************************/
var ua = new ua_classname();
ua.init();

//alert($("html").attr("class"))

}); // END document.ready


//<ul data-equal-height=".used-equipment-equal-height" data-equal-height-nth="3">
function equal_height(selector,target,nth)
{
	var equal_height_parent = $(selector);
	
	var equal_height_target = $(equal_height_parent.attr("data-equal-height"));
	if(target != false) { equal_height_target = target; }
	
	var equal_height_nth = parseInt(equal_height_parent.attr("data-equal-height-nth"));
	if(nth != false) { equal_height_nth = nth; }
	
	equal_height_parent.find(equal_height_target).setMinHeight(equal_height_nth).addClass("equalheight");
}