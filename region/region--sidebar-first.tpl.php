<?php 
	//This is the side bar that appears to the left of internal pages - not used
	
?>
<aside class="fixed <?php print($classes) ?>">
	<h3 class="heading lined margin-top-none"><?php print(drupal_get_title()); ?></h3>
	<?php print $content ?>
</aside>