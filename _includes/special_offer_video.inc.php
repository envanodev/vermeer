<?php if ($video): ?>
<div class="watch-video">
    <div class="watch-video-container">
        <h3><?php echo $title; ?></h3>
        <?php if (isset($youtube_id)): ?>
        <div class="videoplayer">
            <iframe width="478" height="267" src="http://www.youtube.com/embed/<?php echo (isset($youtube_id) ? $youtube_id : ''); ?>" frameborder="0"></iframe>
        </div>
        <?php elseif ($video): ?>
            <div class="videoplayer-specialoffer">
                <p class="video-url"><?php echo $video; ?></p>
                <div class="video-container" id="special-offer-media-video-<?php echo $row->_field_data['nid']['entity']->nid; ?>"></div>
            </div>
        <?php endif; ?>
        <?php echo (isset($video_caption) ? '<p class="caption">' . $video_caption . '</p>' : '') ?> 
    </div>
</div>
<?php endif; ?>
