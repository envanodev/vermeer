<footer class="footer"> 
	<div class="container-inside">
				
		<div class="footer-logo"><a href="http://www.equippedtodomore.com/"><img src="files/images/template/logo-footer.png" alt="Vermeer Equipped to do More"></a></div>
		
		<div class="footer-links overflow footer-text">
			<ul class="reset content-left margin-right-4x margin-bottom-1x">
				<li><a href="index.php">Home</a></li>
				<li><a href="new-equipment.php">New Equipment</a></li>
				<li><a href="used-equipment.php">Used Equipment</a></li>
				<li><a href="style-guide.php">Parts &amp; Accessories</a></li>
				<li><a href="style-guide.php">Service</a></li>
				<li><a href="style-guide.php">Special Offers</a></li>
				<li><a href="style-guide.php">Locations</a></li>
			</ul>

			<ul class="reset content-left margin-bottom-1x">
				<li><a href="about.php">About Us</a></li>
				<li><a href="style-guide.php">Careers</a></li>
				<li><a href="contact.php">Contact Us</a></li>
				<li><a href="events.php">Events</a></li>
				<li><a href="find-a-sales-rep.php">Find a Sales Rep</a></li>
				<li><a href="style-guide.php">Finance &amp; Credit</a></li>
				<li><a href="news.php">Newsroom</a></li>
				<li><a href="style-guide.php">Now Hiring</a></li>
			</ul>
		</div>
		
		<!-- Optional, Some have 0,1,2,3,4 -->
		<ul class="social collapse margin-top-1x margin-bottom-1x">
			<li><a class="icon-facebook before" href="http://www.facebook.com/"><span>Facebook</span></a></li>
			<li><a class="icon-twitter before" href="http://twitter.com/"><span>Twitter</span></a></li>
			<li><a class="icon-youtube before" href="http://www.youtube.com/"><span>Youtube</span></a></li>
			<li><a class="icon-linkedin before" href="http://www.linkedin.com/"><span>LinkedIn</span></a></li>
		</ul>
		
		<div class="legal footer-text"><a href="style-guide.php">Terms &amp; Conditions</a> •  Copyright Vermeer <?=date("Y")?></div>

	</div> <!-- .container-inside -->
</footer> <!-- .footer -->

</div> <!-- .container -->

<!--
<div class="nav-opaque-layer event-nav-view" title="Open/Close"></div>
-->

</body>
</html>