<?php
    /*
		Notes: Template for individual Used Equipment item.
	*/
    // Get Referer URL
    $back_to_search = FALSE;
    $referer = $_SERVER['HTTP_REFERER'];
    
    // If Referer URL is Used Equipment Search
    if (strpos(request_path(), 'used-equipment/') !== false && request_path() != 'used-equipment' && preg_match('/used-equipment([?#](.*))?/', $referer)) {
        
        // Create Referer URL
        $referer_url = explode('.com', $referer);
        $referer_url = $referer_url[1];
        
        // Back to Search is Visible
        $back_to_search = TRUE;
    }
?>

<section class="content-span" data-cs-padding="2 0" data-cs-theme="white">
	
	<div class="container-inside">
				<?php if ($tabs && (!isset($node) || (isset($node) && ($node->type != 'special_offers' || !$node->domain_site)))) { ?>
				    <div class="admin-tabs"><?php print render($tabs); ?></div>
				<?php } ?>
		<?php if ($title) { ?>
		    <?php if ($back_to_search == TRUE) { ?>
		        <h3 class="heading margin-top-none"><a href="<?php echo $referer_url; ?>" class="content-right unbold no-wrap" data-unfloat-bp="tabletportrait"><?php print t('Back to Results'); ?></a></p>
		    <?php } ?>
		<?php } ?>
		<?php print $breadcrumb?>
		<?php print render($page['content']); ?>


	</div> <!-- .container-inside -->
</section> <!-- .content-span -->
<section class="content-span" data-cs-padding="2 0" data-cs-theme="gray-2">
	<div class="container-inside">
	
		<h3 class="heading lined margin-top-none margin-bottom-2x">
			Gallery
			<div class="content-right unbold" data-font-size="-1">Click any image to launch the Gallery</div>
		</h3>
		<div class="imageContainer">
			<h3 class="no-images align-center">No Images</h3>
		</div>
		<div class="align-center hidden" data-show-breakpoint="phablet">
			<a href="#view-all-photos" class="button event-view-all-photos" data-theme="yellow" data-size="large" tabindex="0">View All Photos</a>
		</div>

	</div> <!-- .container-inside -->
</section> <!-- .content-span -->	
<script>
	
	$(window).ready(function(){
		//Use body class to output this to the right page of just added to the tamplate.php file
		if($('#prerenderedImages').length) {
			$( "#prerenderedImages" ).appendTo( ".imageContainer" ).fadeIn();
			if($( "#prerenderedImages li").length < 6) {
				$('.event-view-all-photos').remove();
			}
			$('.no-images').hide();
		} else if($( "#prerenderedImages li").length < 6) {
			$('.event-view-all-photos').remove();
		}
		
		
				
	});
</script>




