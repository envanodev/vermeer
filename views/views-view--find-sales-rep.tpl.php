<div class="<?php print $classes; ?>">
  <?php if ($rows) { ?>
    <div class="padding-4x bottom top  view-content">
      <?php print $rows; ?>
    </div>
  <?php } elseif ($empty) { ?>
    <div class="padding-4x bottom top  view-empty">
      <?php print vermeer_var('sales_rep_no_results_message'); ?>
    </div>
  <?php } ?>
</div>
