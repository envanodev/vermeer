<?php
/**
 * @file field--field_offers_expiration_date.tpl.php
 * Template for special offers expiration date field.
 */
?>


<?php foreach ($items as $delta => $item): ?>
  <p class="date"><?php echo t('Expires'); ?> <?php print render($item); ?></p>
<?php endforeach; ?>
