<?php global $_domain; ?>
<script>
	
	//code to prevent the reset button from going to the single page view and to reset all of the filters
	$(window).ready(function(){
		//console.log($('.node-type-used-equipment-overview #edit-reset'));
		var $resetBtn = $('.node-type-used-equipment-overview #edit-reset');
		var form = $resetBtn.closest('form')[0];
		$resetBtn.click(function(button){
			form.reset();
            $('#edit-submit-used-equipment').click();
			button.preventDefault();
			
		})
		
	})
</script>
<aside class="main content-left margin-right-2x margin-bottom-4x" data-unfloat-bp="phablet" data-full-width-bp="phablet">

	<div class="accordion-group tabs-mobile" data-theme="icons">
		<ul class="tabs reset group">
			<li class="selected"><a href="#panel-search" class="icon-search2 before"><span class="hidden">Search</span></a></li>
			<?php 
			    if(isset($content['field_one_click_searches']['#items'])) {
			?>
			<li><a href="#panel-popular" class="icon-star before"><span class="hidden">Popular Searches</span></a></li>
			<?php 
				}
			?>
			<?php if($_domain['domain_id'] != 19) { ?>
			<li><a href="#panel-rep" class="icon-person before"><span class="hidden">Find Your Rep</span></a></li>
			<?php } ?>
		</ul>
		<dl class="accordion reset">
			<dt class="selected"><a href="#panel-search">Search</a></dt>
			<dd class="selected">
				
				<div class="used-equipment-search margin-bottom-4x">
					<h3 class="heading lined margin-top-none margin-bottom-2x">Search</h3>					
					<?php 
						
						      // Embed Used Equipment Search
			        $used_equipment_overview = 'used_equipment';
			        $used_equipment_overview_view = views_get_view($used_equipment_overview);
					
					$display_id = 'block';
					$used_equipment_overview_view->set_display($display_id);
					$used_equipment_overview_view->init_handlers(); //initialize display handlers
					$form_state = array(
					'view' => $used_equipment_overview_view,
					'display' => $used_equipment_overview_view->display_handler->display,
					'exposed_form_plugin' => $used_equipment_overview_view->display_handler->get_plugin('exposed_form'), //exposed form plugins are used in Views 3
					'method' => 'get',
					'rerender' => TRUE,
					'no_redirect' => TRUE,
					);
					$form = drupal_build_form('views_exposed_form', $form_state);
					$he = drupal_render($form);
					print($he);
			
					?>
				</div>
				
			</dd>
			<?php 
			    if(isset($content['field_one_click_searches']['#items'])) {
			?>
			<dt><a href="#panel-popular">Popular Searches</a></dt>
			<dd>
				<div class="used-equipment-popular margin-bottom-4x">					
					<h3 class="heading lined margin-top-none margin-bottom-2x">Popular Searches</h3>
					<ul class="reset children-margin-bottom-half">
				<?php
			            foreach($content['field_one_click_searches']['#items'] as $type) {
			                if ($type['value'] != '0') {
			                    if ($title = vermeer_used_equipment_translate_type($type['safe_value'])) {
			                        print '<li><a class="button-popular"  href="?type=' . urlencode($type['safe_value']) . '">' . $title . '</a></li>';
			                    }
			                }
			            }
			    ?>
					</ul>
				</div>							

			</dd>
			<?php         
			    }
			?>
			<?php if($_domain['domain_id'] != 19) { //FIX LATER: TEMP FIX?>
			

			<dt><a href="#panel-rep"><?php t('Find Your Rep') ?></a></dt>
			<dd>
				
				
										
					<?php 
						
							 // Embed Sales Rep Search
							$view = views_get_view('find_sales_rep');
							$display_id = 'page_3';
							$view->set_display($display_id);
							$view->init_handlers(); //initialize display handlers
							$form_state = array(
							'view' => $view,
							'display' => $view->display_handler->display,
							'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'), //exposed form plugins are used in Views 3
							'method' => 'get',
							'rerender' => TRUE,
							'no_redirect' => TRUE,
							);
							$form = drupal_build_form('views_exposed_form', $form_state); //create the filter form
							//you now have a form array which can be themed or further altered...
							$he = drupal_render($form);
							print $he;
														      
							      
							      
					
					?>
				
			</dd>
			<?php } ?>
		</dl>
	</div>

</aside>	

<div class="overflow" data-ajax-throbber-custom="true">
	
	<h3 class="heading lined margin-top-none margin-bottom-2x">
		Results
	</h3>


    
    <?php
      // Embed Hot Listing
      $view = views_get_view('hot_listing');
      $view->set_display('block');
      $hot_listing = $view->render();
      if (count($view->result)) {
        $class = ' split-width';
      }
      else {
        $class = ' full-length';
      }

      // Print Intro Copy
      if(!empty($content['field_intro']['#object']->field_intro['und']['0']['safe_value'])) {
	      print($content['field_intro']['#object']->field_intro['und']['0']['safe_value']); 
      }

      // Print Hot Listing
/*
      if (count($view->result)) {
        print $hot_listing;
      }
*/
    ?>
    
    <?php
	  	print views_embed_view('used_equipment', 'block'); 
    ?>
</div>
