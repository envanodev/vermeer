<?php

    // Title
    $title = $row->_field_data['nid']['entity']->title;
    
    // Node Path
    $nid = 'node/' . $row->_field_data['nid']['entity']->nid;
    //$path = l('Read more &#187;', $nid, array('html' => TRUE));
    $path = url($nid);
    
    // Video
    if(isset($row->_field_data['nid']['entity']->field_offers_video_url['und'][0])) {
        $video = $row->_field_data['nid']['entity']->field_offers_video_url['und'][0]['value'];
        $youtube_id = vermeer_youtube_parse($video);
    }
    elseif(isset($row->_field_data['nid']['entity']->field_offers_video_source['und'][0])) {
        $video = $row->_field_data['nid']['entity']->field_offers_video_source['und'][0]['value'];
    }
    else {
        $video = NULL;
    }
    
    // Video Caption
    if(isset($row->_field_data['nid']['entity']->field_offers_video_caption['und'][0])) {
        $video_caption = $row->_field_data['nid']['entity']->field_offers_video_caption['und'][0]['value'];
    }
    
    // Compare Short Description and Full Description
    if(isset($row->_field_data['nid']['entity']->field_offers_short_desc['und'][0])) {
        $short_description = $row->_field_data['nid']['entity']->field_offers_short_desc['und'][0]['value'];
    }
    if(isset($row->_field_data['nid']['entity']->body['und'][0])) {
        $full_description = strip_tags($row->_field_data['nid']['entity']->body['und'][0]['value'], '');
    }
    else {
        $full_description = NULL;
    }
?>


<p><?php echo $short_description; ?></p>
<?php if ($video): ?>
    <p class="video"><a class="new-window-ignore"  rel="shadowbox[gallery]" href="http://www.youtube.com/embed/<?php echo (isset($youtube_id) ? $youtube_id : ''); ?>"><?php echo t('Watch Video'); ?> &#187;</a></p>
<?php endif; ?>
<?php if (isset($full_description) && $short_description !== $full_description): ?>
    <p>
   	 <a href="<?php echo $path; ?>" class="button" data-theme="yellow" data-size="large" tabindex="0">Learn More &#187;</a>
    </p>
<?php endif; ?>
<?php //include(drupal_get_path('theme', 'vermeer') . '/_includes/special_offer_video.inc.php'); ?>
