
<?php
	
// This is the wrapper used throught the site. 
// Header, nav, footer, and globals are located here.
// This is everything inside the body element.
//
//
//
    global $_domain;

    // Switch between h1 and h2 for the site title
    $logo_open = ($is_front) ? "<h1>\n" : "<h2>\n";
    $logo_close = ($is_front) ? "</h1>\n" : "</h2>\n";
    
    // Site logo and name
    $sitename = $_domain['sitename'];
    $sitelogo = file_load(vermeer_var('logo'));
    $sitelogo = ($sitelogo) ? '/sites/default/files/' . $_domain['machine_name'] . '/_images/logo/' . $sitelogo->filename : '/sites/all/themes/vermeer/logo.png';
    
    // Contact info and top utility bar
    $utility_var = vermeer_var('utility_bar');
    $contact_phone_1 = vermeer_var('phone_number_1');
    $contact_phone_2 = vermeer_var('phone_number_2');
    $contact_email = vermeer_var('email_address');
    $contact_label = vermeer_var('contact_label');
    $contact_label_url = vermeer_var('contact_label_url');
    $contact_address = vermeer_var('contact_address');
    
    //Delete
    $contact_info_header = vermeer_var('header_contact_info');
    $contact_info_footer = vermeer_var('footer_contact_info');


    // Footer Social Media
    $facebook = htmlentities(vermeer_var('facebook'), ENT_QUOTES);
    $twitter = htmlentities(vermeer_var('twitter'), ENT_QUOTES);
    $youtube = htmlentities(vermeer_var('youtube'), ENT_QUOTES);
    $linkedin = htmlentities(vermeer_var('linkedin'), ENT_QUOTES);
	
	// Used Equipment View
    $used_equipment_toggle = vermeer_var('used_equipment_carousel');
    if($used_equipment_toggle == 0) {
        $used_equipment_view = 'vermeer_used_equipment_home_slider';
        $used_equipment_view_view = views_get_view($used_equipment_view);
        $used_equipment_view_view->execute();
        // Count the number of rows
        $used_equipment_view_count = count($used_equipment_view_view->result);
        
        // Store the rendered HTML markup
        if($used_equipment_view_count >= 4) {
            $used_equipment_view_html = views_embed_view($used_equipment_view, 'block');
        }
        else {
            $used_equipment_view_html = FALSE;
        }
    }
    
    // Detect if we're under the Used Equipment section
/*
    if($used_equipment_toggle == 0) {
        $current_path = explode('/', urldecode(request_uri()));
        $used_equipment_path = NULL;
        if ($current_path[1] == 'used-equipment') {
            $used_equipment_path = TRUE;
        }
    }
*/
    $used_equipment_path = NULL;
    if(isset($node) && $node->type == "used_equipment_overview") {
            $used_equipment_path = TRUE;
    }
    
    // Mobile Intercept
    $mobile_headline = '';
    $mobile_description = '';
    $mobile_phone = '';
    //this is the home page of each site 
    $front_page = variable_get('site_frontpage', 'node');
    $front_page = explode('node/', $front_page);
    $front_nid = $front_page[1];
    $front_node = node_load($front_nid);
    if ($front_node->type == 'home_page') {
        $mobile_headline = (isset($front_node->field_mobile_headline['und'][0]['safe_value']) ? $front_node->field_mobile_headline['und'][0]['safe_value'] : '');
        $mobile_description = (isset($front_node->field_mobile_description['und'][0]['safe_value']) ? $front_node->field_mobile_description['und'][0]['safe_value'] : '');
        $mobile_phone = (isset($front_node->field_mobile_phone['und'][0]['safe_value']) ? $front_node->field_mobile_phone['und'][0]['safe_value'] : '');   
        if (isset($front_node->field_mobile_cta_link_type['und'][0]['value']) && $front_node->field_mobile_cta_link_type['und'][0]['value'] == 'Node Link') {
            if ($front_node->field_mobile_cta_link_type['und'][0]['value'] == 'Node Link' && isset($front_node->field_mobile_cta_node_link['und'][0]['nid'])) {
                $mobile_cta_path = 'node/' . $front_node->field_mobile_cta_node_link['und'][0]['nid'];
            }
            if ($front_node->field_mobile_cta_link_type['und'][0]['value'] == 'Path Link' && isset($front_node->field_mobile_cta_path_link['und'][0]['value'])) {
                $mobile_cta_path = $front_node->field_mobile_cta_path_link['und'][0]['value'];
            }
            if (isset($front_node->field_mobile_cta_link_text['und'][0]['value'])) {
                $mobile_cta_text = $front_node->field_mobile_cta_link_text['und'][0]['value'];
            }
            if (isset($mobile_cta_path) && isset($mobile_cta_text)) {
                $mobile_cta = l($mobile_cta_text, $mobile_cta_path);
            }
        }
    }
    
?>


<div class="container">

	<?php 
	//if the utility option is on ... then show it	
	if($utility_var) { 
		
	?>

	<section  class="utility-bar group promote-layer">
		<div  class="container-inside">
			
			<!-- Optional -->
		<?php if(!empty($contact_phone_1)) { ?>
			<div id="step-1" class="utility-bar-phone utility-bar-item">
				
				<a target="_blank" data-track-click='[{"category": "Menu", "label": "Phone Number"}]' href="tel://<?php echo(str_replace(array(' ', '-','(',')','.','_'),'',$contact_phone_1)) //remove and spaces and symbols from the content ?>" class="icon-phone before"><?php echo($contact_phone_1) ?></a> 
				<?php if(!empty($contact_phone_2)) { ?>
				 or <a target="_blank" data-track-click='[{"category": "Menu", "label": "Phone Number"}]' href="tel://<?php echo(str_replace(array(' ', '-','(',')','.','_'),'',$contact_phone_2)) ?>"><?php echo($contact_phone_2) ?></a>
				 <?php } ?>
			</div>
		<?php } ?>
		<?php if(!empty($contact_email)) { ?>
			<!-- Optional -->
			<div class="utility-bar-email utility-bar-item">
				<a  data-track-click='[{"category": "Menu", "label": "Email Address"}]' href="mailto:<?php echo($contact_email) ?>" class="icon-email before"><?php echo($contact_email) ?></a>
			</div>
		<?php } ?>
		<?php if(!empty($contact_label)) { ?>
			<!-- Optional - http://vermeermidwest.com/ -->
			<div class="utility-bar-link utility-bar-item">
				<a data-track-click='[{"category": "Menu", "label": "Contact Link"}]' href="<?php if(isset($contact_label_url)) {echo($contact_label_url);} ?>"><?php echo($contact_label) ?> &raquo;</a>
			</div>
		<?php } ?>
		<?php if(!empty($contact_address)) { ?>
			<div class="utility-bar-address utility-bar-item">
				<a href="#"><?php echo($contact_address) ?></a>
			</div>
		<?php } ?>
			<aside class="utility-bar-aside group">
				<!-- Optional - Language Dropdown -->
				<?php echo render($page['header']); ?>
				<?php 
				// Optional, Some have 0,1,2,3,4 
				if(!empty($facebook) || !empty($twitter) || !empty($youtube) || !empty($linkedin)) {
				?>	
				<ul class="social collapse">
                <?php if ($facebook) { ?>
                    <li><a data-track-click='[{"category": "Menu", "label": "Facebook Link"}]' class="icon-facebook before" target="_blank" href="<?php echo $facebook; ?>"><span>Facebook</span></a></li>
                <?php } ?>
                <?php if ($twitter) {?>
                    <li><a data-track-click='[{"category": "Menu", "label": "Twitter Link"}]' class="icon-twitter before" target="_blank" href="<?php echo $twitter; ?>"><span>Twitter</span></a></li>
                <?php } ?>
                <?php if ($youtube) { ?>
                    <li><a data-track-click='[{"category": "Menu", "label": "Youtube Link"}]' class="icon-youtube before" target="_blank" href="<?php echo $youtube; ?>"><span>YouTube</span></a></li>
                <?php } ?>
                <?php if ($linkedin) { ?>
                    <li><a data-track-click='[{"category": "Menu", "label": "LinkedIn Link"}]' class="icon-linkedin before"  target="_blank" href="<?php echo $linkedin; ?>"><span>LinkedIn</span></a></li>
                <?php } ?>
				</ul>
				<?php } ?>
			</aside> <!-- .utility-bar-aside -->
	
		</div> <!-- .container-inside -->
	</section> <!-- .utility-bar -->
<?php } ?>
	<section class="faux-header"> <!-- event-nav-view -->
		<div class="container-inside">
		
			<nav class="nav-mobile">	
				<ul class="nav-list group" data-copy-node-source=".nav .nav-list > li">
					<li class="static nav-padding nav-divider" data-copy-node-source=".utility-bar-phone"></li>
					<li class="static nav-padding" data-copy-node-source=".utility-bar-email"></li>
					<li class="static nav-padding" data-copy-node-source=".utility-bar-language"></li>
					<li class="static" data-copy-node-source=".utility-bar .social"></li>
				</ul>	
			</nav>
		
		</div> <!-- .container-inside -->
		
		<div class="nav-opaque-layer event-nav-view"></div>
		
	</section> <!-- .faux-header -->
	<header class="header group">
		<div  class="container-inside">
			<h1  ><a href="/"><img src="<?php echo $sitelogo; ?>" alt="[<?php echo $sitename; ?>]" title="<?php echo $sitename; ?>"></a></h1>
			
			<a href="#nav-open" class="mobile-header-button mobile-nav-button icon-tribar before event-nav-view"><span>Open</span></a>		
			<a href="#nav-close" class="mobile-header-button mobile-nav-button icon-close before event-nav-view"><span>Close</span></a>
			
			<nav id="main-nav" class="nav">		
				<?php
					//The main navigation function is on the ../template.php files
					 echo vermeer_main_menu($main_menu); 
				?>
			</nav> <!-- .nav -->
			<?php if(!empty($contact_phone_1)) { ?>
			<a target="_blank" data-track-click='[{"category": "Mobile Menu", "label": "Phone Number"}]'  href="tel://<?php echo(str_replace(array(' ', '-','(',')','.','_'),'',$contact_phone_1)) //remove and spaces and symbols from the content ?>" class="mobile-header-button mobile-phone-button icon-phone before"><span class="hide"><?php echo($contact_phone_1) ?></span></a>
			<?php } ?>
		</div> <!-- .container-inside -->
	</header> <!-- .header -->
<!--  Main Content	 -->
<main class="content">
<?php 
	//output main content	   
	echo $content; 
		   
?>

						

<?php if($used_equipment_toggle == 0 && $used_equipment_view_html && $used_equipment_path == NULL) { ?>
<!-- Used Equipment Carousel  -->
				<?php if(isset($node)) { ?>
				<?php if($node->type == 'home_page' || $node->type == 'used_equipment') {  ?>
				<section class="content-span" data-cs-padding="2 0" data-cs-theme="white">
				<?php } else { ?>
				<section  class="content-span" data-cs-padding="2 0" data-cs-theme="gray-2" data-cs-border="1 0 0 0">
				<?php } ?>
                <?php echo $used_equipment_view_html; ?>
				</section> <!-- .content-span -->
				<?php } ?>

<!-- End Used Equipment Carousel  -->
<?php } ?>
      
				
			
<!-- Footer Parallax CTA -->
        <?php if (isset($node) && $node->type == 'home_page' && $_domain['domain_id'] != 1) { ?>
            <?php if (footerparallaxcta($node)) { ?>
            		<div >
    			 <?php echo footerparallaxcta($node); ?>
    			 	</div>
    		
    		<?php } ?>
        <?php } ?>
<!-- End Footer Parallax CTA -->
<!-- 		Home Resources Callouts -->
        <?php if (isset($node) && $node->type == 'home_page' && $_domain['domain_id'] != 1) { ?>
            <?php if (vermeer_home_resources($node)) { ?>
    			 <?php echo vermeer_home_resources($node); ?>
    		<?php } ?>
        <?php } ?>
		
<!-- 		end Home Resources Callouts -->
</main>	
<!--  End Main Content	 -->
<!-- Footer -->
	<footer class="footer"> 
		<div class="container-inside">
			<ul class="columns columns-2" data-bp="mobilelandscape">
				<li class="column-row">
					<div class="footer-links overflow footer-text">
		<!-- 			Primary Nav Footer	 -->
		                <?php echo vermeer_footer_main_menu($main_menu); ?>
		<!-- 			end Primary Nav Footer	 -->
		<!-- Secondary nav  -->
		<!-- end Secondary nav  -->
		                <?php echo vermeer_secondary_menu($secondary_menu); ?>
					</div>
				</li>
				<li>
					<aside class="footer-aside">
						<!-- Optional, Some have 0,1,2,3,4 -->
						<?php
						if(!empty($facebook) || !empty($twitter) || !empty($youtube) || !empty($linkedin)) {	
						?>
						<ul class="social reset margin-bottom-1x">
			                <?php if ($facebook) { ?>
			                    <li><a class="icon-facebook before" target="_blank" href="<?php echo $facebook; ?>"><span>Facebook</span></a></li>
			                <?php } ?>
			                <?php if ($twitter) {?>
			                    <li><a class="icon-twitter before" target="_blank" href="<?php echo $twitter; ?>"><span>Twitter</span></a></li>
			                <?php } ?>
			                <?php if ($youtube) { ?>
			                    <li><a class="icon-youtube before" target="_blank" href="<?php echo $youtube; ?>"><span>YouTube</span></a></li>
			                <?php } ?>
			                <?php if ($linkedin) { ?>
			                    <li><a class="icon-linkedin before"  target="_blank" href="<?php echo $linkedin; ?>"><span>LinkedIn</span></a></li>
			                <?php } ?>
						</ul>
						<?php } ?>
						<!-- 	Third menu and site map		 -->
						<div id="step-tertiary-nav" class="legal footer-text"><?php echo vermeer_teritary_menu($_domain); ?><?php echo t('Copyright'); ?> <?php echo $sitename . ' ' . date('Y'); ?></div>

					</aside>
				</li>
			</ul>
			
 		</div> <!-- .container-inside -->
	</footer> <!-- .footer -->

</div> <!-- .container -->
<?php if($_domain['domain_id'] == 35) { ?>
<!-- Tutorial -->
	<a id="start-tour" href="#start"><span class="before icon-star"></span> Start Tour</a>
	<ol style="display: none" id="siteTour">
	  <li data-id="step-1" data-options="tipLocation:bottom;tipAnimation:fade" data-text="Next">
        <h4>Utility Bar</h4>
        <p>The Utility Bar contains a dealer’s contact information. It supports up to four types of information: Phone Number, Email, Link to a Page, and Location Address.</p>
	  </li>
	  <li data-id="main-nav" data-options="tipLocation:bottom;tipAnimation:fade" data-button="Next" data-text="Next">
        <h4>Main Navigation</h4>
        <p>The Main Navigation supports up to seven items. Adding a link within items is possible.</p>
	  </li>
	  <li data-id="step-home-marquee" data-options="tipLocation:right;tipAnimation:fade" data-button="Next" data-text="Next">
        <h4>Home Page Marquees</h4>
        <p>The new theme supports up to six marquees. Images should not contain any text since you have the ability to add a title, description and a link to each marquee.</p>
	  </li>
	  <li data-id="step-used-equipment-carousel" data-options="tipLocation:top;tipAnimation:fade" data-button="Next">
        <h4>Used Equipment Carousel</h4>
        <p>The Used Equipment Carousel displays four items at a time, and site visitors can advance forward or backward through the carousel with the arrow buttons on either side. The carousel will display a maximum of 16 pieces of equipment sorted by date, with the most recently added content appearing near the top of the list.</p>
	  </li>
	  <li data-id="step-footer-cta" data-options="tipLocation:top;tipAnimation:fade">
        <h4>Footer Call To Action</h4>
        <p>The Footer Call To Action can be used to link content on your site. A background image can be added to match the link along with the title and description.</p>
	  </li>
	  <li data-id="slide-callouts" data-options="tipLocation:bottom;tipAnimation:fade">
	  	<h4>Home Resources Callouts</h4>
	  	<p>The Home Resources Callouts can be used to link to other pages, articles, or promotions on your site. There are three available resource callouts to add on this page.</p>
	  </li>
	  <li data-id="step-secondary-nav" data-options="tipLocation:right;tipAnimation:fade">
	  	<h4>Secondary Navigation</h4>
	  	<p>The Secondary Navigation is used for less prominent content supporting internal and external links.</p>
	  	
	  </li>
	  <li data-id="step-tertiary-nav" data-options="tipLocation:top;tipAnimation:fade" data-button="Done">
	  	<h4>Tertiary Menu</h4>
	  	<p>The Tertiary Menu is an Inline list of links. Content such as the privacy policy and terms and conditions appear in this menu.</p>
	  	
	  </li>
	</ol>
<?php } ?>



