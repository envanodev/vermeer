</head>

<!-- This is set if the utility bar is hidden for a dealer data-utility-bar: true | false -->
<body id="specificity" data-utility-bar="true">

<div class="container">

<!-- This whole bar could be hidden - http://vermeerhl.com/ -->
<section class="utility-bar group promote-layer">
	<div class="container-inside">
		
		<!-- Optional -->
		<div class="utility-bar-phone utility-bar-item">
			<a href="files/telephone/?tel=8005551111" class="icon-phone before">(800) 624-0623</a> or <a href="files/telephone/?tel=8005551111">(609) 267-6600</a>
		</div>
		
		<!-- Optional -->
		<div class="utility-bar-email utility-bar-item">
			<a href="mailto:pella.ia@vermeeriowa.com" class="icon-email before">pella.ia@vermeeriowa.com</a>
		</div>
		
		<!-- Optional - http://vermeermidwest.com/ -->
		<div class="utility-bar-link utility-bar-item">
			<a href="contact.php">Ask Us a Question &raquo;</a>
		</div>
		
		<!-- Optional - http://vermeersouthwest.com/ 
		<div class="utility-bar-address utility-bar-item">
			<a href="#">436 S. Hamilton Ct. Gilbert, AZ 85233</a>
		</div>
		-->
		
		<aside class="utility-bar-aside group">
			<!-- Optional - http://fr.vermeercanada.vermeerdealersystem.com/ -->
			<dl class="utility-bar-language reset">
				<dt class="event-panel-toggle-menu unselectable" data-panel-toggle-target=".utility-bar-languages"><span class="icon-arrow-down after">English</span></dt>
				<dd class="utility-bar-languages hidden" data-offclick-close="true">
					<ul class="reset">
						<li><a href="#">English</a></li>
						<li><a href="#">French</a></li>
					</ul>
				</dd>
			</dl>
			
			<!-- Optional, Some have 0,1,2,3,4 -->
			<ul class="social collapse">
				<li><a class="icon-facebook before" href="http://www.facebook.com/"><span>Facebook</span></a></li>
				<li><a class="icon-twitter before" href="http://twitter.com/"><span>Twitter</span></a></li>
				<li><a class="icon-youtube before" href="http://www.youtube.com/"><span>Youtube</span></a></li>
				<li><a class="icon-linkedin before" href="http://www.linkedin.com/"><span>LinkedIn</span></a></li>
			</ul>
		</aside> <!-- .utility-bar-aside -->

	</div> <!-- .container-inside -->
</section> <!-- .utility-bar -->


<section class="faux-header"> <!-- event-nav-view -->
	<div class="container-inside">
	
		<nav class="nav-mobile">	
			<ul class="nav-list group" data-copy-node-source=".nav .nav-list > li">
				<li class="static nav-padding nav-divider" data-copy-node-source=".utility-bar-phone"></li>
				<li class="static nav-padding" data-copy-node-source=".utility-bar-email"></li>
				<li class="static nav-padding" data-copy-node-source=".utility-bar-language"></li>
				<li class="static" data-copy-node-source=".utility-bar .social"></li>
			</ul>	
		</nav>
	
	</div> <!-- .container-inside -->
	
	<div class="nav-opaque-layer event-nav-view"></div>
	
</section> <!-- .faux-header -->


<header class="header group">
	<div class="container-inside">
		<h1><a href="index.php"><img src="files/images/fpo/header-logo.png" alt="[INSERT DEALER NAME HERE]"></a></h1>
		
		<a href="#nav-open" class="mobile-header-button mobile-nav-button icon-tribar before event-nav-view"><span>Open</span></a>		
		<a href="#nav-close" class="mobile-header-button mobile-nav-button icon-close before event-nav-view"><span>Close</span></a>
		
		<nav id="main-nav" class="nav">			
			<ul class="nav-list group">
				<!-- Constraint: Max of 7 nav items -->
				<li class="dynamic selected has-children" aria-haspopup="true"><a href="about.php"><span class="nav-vertical-align"><span>About Vermeer Sales &amp; Service, Inc.</span></span></a>
					<ul>
						<li class="hide"><a href="about.php">About Vermeer Sales &amp; Service, Inc.</a></li> <!-- This should be a duplicate of the parent nav item. This would only be present if there's a subnav -->
						<li><a href="about.php">Alamo</a></li>
						<li><a href="about.php">Amarillo</a></li>
						<li><a href="about.php">Austin (Round Rock)</a></li>
						<li><a href="about.php">Corpus Christi</a></li>
						<li><a href="about.php">Dallas/Fort Worth (Irving)</a></li>
						<li><a href="about.php">Denham Springs</a></li>
						<li><a href="about.php">Houston (Cypress)</a></li>
						<li><a href="about.php">Kilgore</a></li>
						<li><a href="about.php">Lubbock</a></li>
						<li><a href="about.php">Midland/Odessa</a></li>
						<li><a href="about.php">San Antonio (Selma)</a></li>
						<li><a href="about.php">Waco (Ross)</a></li>
					</ul>
				</li>
				<li class="dynamic"><a href="new-equipment.php"><span class="nav-vertical-align"><span>New Equipment</span></span></a></li>
				<li class="dynamic"><a href="used-equipment.php"><span class="nav-vertical-align"><span>Used Equipment</span></span></a></li>
				<li class="dynamic"><a href="about.php"><span class="nav-vertical-align"><span>About Us</span></span></a></li>
				<li class="dynamic"><a href="news.php"><span class="nav-vertical-align"><span>News</span></span></a></li>
				<li class="dynamic"><a href="events.php"><span class="nav-vertical-align"><span>Events</span></span></a></li>
				<!--<li class="dynamic"><a href="contact.php"><span class="nav-vertical-align"><span>Contact</span></span></a></li>-->
				<li class="dynamic"><a href="http://www.gopaperless.com/"><span class="nav-vertical-align"><span>Go Paperless&nbsp;<span class="icon-external before"></span></span></span></a></li>
			</ul>
		</nav> <!-- .nav -->
		
		<a href="files/telephone/?tel=8005551111" class="mobile-header-button mobile-phone-button icon-phone before"><span class="hide">(800) 624-0623</span></a>
		
	</div> <!-- .container-inside -->
</header> <!-- .header -->