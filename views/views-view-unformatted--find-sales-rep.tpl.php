<?php
	
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 * Used on the find a sales rep page
 */
?>
<ul class="columns columns-2 equal-columns" data-bp="mobilelandscape">
<?php if (!empty($title)) { ?>
  <h3><?php print $title; ?></h3>
<?php } ?>
<?php $counter = 0; ?>
<?php foreach ($rows as $id => $row) { ?>
	
  <li class="padding-4x bottom <?php if($counter%2 == 0) { echo(' column-row '); }  if ($classes_array[$id]) { print $classes_array[$id];  } ?> equalheight">
  	<div class="box bordered">
    <?php print $row; ?>
    </div>
  </li>
<?php $counter++; ?>
<?php } ?>
</ul>