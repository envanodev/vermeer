<?php 

$theme_path = dirname(__FILE__);

ob_start();

if(isset($node) && $node->type == "home_page") {
	require $theme_path . '/templates/page-front.tpl.php';
} elseif(isset($node) && $node->type == "used_equipment_overview") {
    require $theme_path . '/templates/page-used-equipment-overview.tpl.php';
} elseif(isset($node) && $node->type == "used_equipment") {
    require $theme_path . '/templates/page-used-equipment-single.tpl.php';
} else {
	 require $theme_path . '/templates/page-default.tpl.php';
}

$content = trim(ob_get_clean());
require $theme_path . '/templates/wrapper.tpl.php';

?>
