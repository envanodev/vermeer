/****************************************************************
	Reponsive class
****************************************************************/
var responsive = responsive || {};
responsive = function ( properties ) {
	//Private properties and methods.
	var _browser_scrollbars_width = 20;
	//var _screen_modifier_width = 1 + _browser_scrollbars_width;
	var _screen_modifier_width = _browser_scrollbars_width;
	var _container_padding = 20 * 2;
	var _width = properties.maxWidth + _screen_modifier_width + _container_padding;
	var _type = properties.classname;
	
	function _viewport_resized(viewport_width)
	{
		if(typeof viewport_width == "object")
		{
			if(viewport_width.matches)
			{
				_media_query_matches(true);
			} else {
				_media_query_matches(false);
			}
		}

		if(typeof viewport_width == "number")
		{
			if(viewport_width < _width)
			{
				_media_query_matches(true);
			} else {
				_media_query_matches(false);
			}
		}
	}	

	function _media_query_matches(is_matched)
	{		
		//For random users that activate menu and then resize while it's still visible
		if($("html").hasClass("nav-open"))
		{
			$(".event-nav-view:first").trigger("nav-view");
		}		
		
		if(is_matched)
		{
			$("html").addClass(_type).removeClass("no-" + _type);
			_bg_image_swap("small");
			_image_swap("small");
			
		} else {
			$("html").addClass("no-" + _type).removeClass(_type);							
			_bg_image_swap("large");
			_image_swap("large");
			_reset_responsive_tabs();
		}	
	}
	
	function _bg_image_swap(size)
	{
		$("[data-image-large][data-image-small]:not(img)").each(function(index, element) {
			var t = $(this);
			var large_image = t.attr("data-image-large");
			var small_image = t.attr("data-image-small");
			if(size == "large") { t.css("background-image", "url(" + large_image + ")"); }
			if(size == "small") { t.css("background-image", "url(" + small_image + ")"); }
		});
	}
	
	function _image_swap(size)
	{
		$("img[data-image-large][data-image-small]").each(function(index, element) {
			var t = $(this);
			var large_image = t.attr("data-image-large");
			var small_image = t.attr("data-image-small");
			if(size == "large") { t.attr("src", large_image); }
			if(size == "small") { t.attr("src", small_image); }
		});
	}

	function _reset_responsive_tabs()
	{
		$(".accordion-group.multiple").each(function(index, element) {
			var root = $(this);
			var activeTabsLength = root.find(".tabs > li.selected").length;
			if(activeTabsLength !== 1)
			{
				var first = root.find(".tabs > li.selected:first a");
				if(first.length === 0) first = root.find(".tabs > li:first a");
	
				root.find(".tabs > li").removeClass("selected");
				root.find(".accordion > dt").removeClass("selected");
				root.find(".accordion > dd").removeClass("selected");
				first.click();
			}
		});
	}

	//Public properties and methods.
	return {		
		init: function() {
			/****************************************************************
				Enable mobile nav or flexible canvas - Note: http://www.stucox.com/blog/you-cant-detect-a-touchscreen/
					Testing for touch
					$("html").removeClass("no-touch");
					$("html").addClass("touch");
			****************************************************************/			
			var cookie = new class_cookies({
				name: "touch",
				expires_as_days: 365,	
				value: "false"
			});
			
			if($.QueryString["touch"] == "false") { cookie.create_cookie(); }
			
			if($.QueryString["touch"] == "true") { cookie.erase_cookie(); }
			
			if(cookie.read_cookie() == "false")
			{
				//Only show desktop version of site
				_media_query_matches(false);
				$("html").removeClass("touch").addClass("no-touch");
			} else {				
				if(window.matchMedia)
				{
					coarse_pointer = window.matchMedia("(pointer: coarse)").matches;
				} else {
					coarse_pointer = false;
				}
				
				if($("html").hasClass("touch") || window.navigator.msMaxTouchPoints || window.navigator.maxTouchPoints || coarse_pointer)
				{
					_media_query_matches(true);
					$("html").removeClass("no-touch").addClass("touch");
				} else if(window.matchMedia) {
					var mql = window.matchMedia("(max-width: " + _width + "px)");
					mql.addListener(_viewport_resized);
					_viewport_resized(mql);
				} else {
					_viewport_resized(this.viewport().width);
				}
			}
		},

		resize: function() {
			if(!window.matchMedia)
			{
				var $this = this;
				//Only call resize function if horizontal width has changed
				var old_viewport_width = $this.viewport().width,
					new_viewport_width = 0;

				$(window).resize(function(){
					new_viewport_width = $this.viewport().width;

					if(old_viewport_width != new_viewport_width && $("html").hasClass("mediaqueries")) //&& $("html").hasClass("touch") == false
					{
						_viewport_resized(new_viewport_width);
					}
					
					old_viewport_width = new_viewport_width;
				});
			} //!window.matchMedia
		},
			
		viewport: function() {
			//http://stackoverflow.com/questions/11309859/media-queries-and-window-width
			var e = window, a = "inner";
			if (!("innerWidth" in window )) {
				a = "client";
				e = document.documentElement || document.body;
			}
			return { width : e[ a+"Width" ] , height : e[ a+"Height" ] };
		}
		
	};
};