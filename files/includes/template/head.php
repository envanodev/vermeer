<!DOCTYPE html> 
<html lang="en" class="no-js no-document-ready no-window-load nav-closed">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">

<link rel="shortcut icon" sizes="196x196" href="files/ico/196x196.png">
<link rel="apple-touch-icon" href="files/ico/60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="files/ico/76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="files/ico/120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="files/ico/152x152.png">

<link rel="shortcut icon" type="image/ico" href="files/ico/favicon.ico">
<link rel="stylesheet" type="text/css" href="files/css/all.css" media="all">

<!--[if lte IE 8]><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><![endif]-->
<!--[if gte IE 9]><script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script><![endif]-->
<!--[if !IE]> --><script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script><!-- <![endif]-->

<script src="files/js/site.min.js"></script>
<!--[if lte IE 8]><script src="files/js/html5.js"></script><![endif]-->

<script>
/* TODO: GOOGLE ANALYTICS */
</script>