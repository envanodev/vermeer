<?php
    /*
		Notes: This template file contains the HTML for the Used Equipment Overview. /used-equipment/     
	*/
    // Get Referer URL
    $back_to_search = FALSE;
    //share equipment Link 
    $shareEquipment = FALSE;
    $referer = $_SERVER['HTTP_REFERER'];
    
    // If Referer URL is Used Equipment Search
    if (strpos(request_path(), 'used-equipment/') !== false && request_path() != 'used-equipment' && preg_match('/used-equipment([?#](.*))?/', $referer)) {
        
        // Create Referer URL
        $referer_url = explode('.com', $referer);
        $referer_url = $referer_url[1];
        
        // Back to Search is Visible
        $back_to_search = TRUE;
        $shareEquipment = TRUE;
    }
?>
<section class="content-span" data-cs-padding="2 0" data-cs-theme="white">
	<div class="container-inside">
		<div class="main-columns fluid-fixed" data-bp="phablet">
				<?php if ($tabs && (!isset($node) || (isset($node) && ($node->type != 'special_offers' || !$node->domain_site)))) { ?>
				    <div class="admin-tabs"><?php print render($tabs); ?></div>
				<?php } ?>
			<div class="fluid-wrapper">
				<div class="overflow event-used-equipment-ajax">
					
					<?php print render($page['content']); ?>
        		</div> <!-- .fluid-wrapper -->

        	</div> <!-- .fluid overflow -->

		</div> <!-- .main-columns -->
	</div> <!-- .container-inside -->
</section> <!-- .content-span -->

<script>	
	$('.event-used-equipment-ajax').ajaxStop(function(){
		equal_height('.view-used-equipment .view-content ul', '.used-equipment-equal-height:not(.equalheight)', 3);
	});
</script>