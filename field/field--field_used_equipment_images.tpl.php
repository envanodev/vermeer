<?php
/**
 * @file field--field_used_equipment_images.tpl.php
 * Template for used equipment image fields.
 */
 	$firstPreviewImage = image_style_url('used_equipment_thumbnail', $items[0]['#item']['uri']);
?>

<?php //Start the wrapper for the top box containing the first image and specs ?>
	<!--  Large image preview 	 -->
    <div class="used-equipment-main-image ratio ratio515_330 content-left margin-right-2x"<?php print $item_attributes[0]; ?> data-full-width-bp="phablet" data-unfloat-bp="phablet" style="background-image: url(<?php echo($firstPreviewImage); ?>)">
		<?php if(!isset($element['#object']->field_used_equipment_images['und'][0]['is_default'])) { ?>
		<a href="<?php echo(image_style_url('used_equipment_full', $items[0]['#item']['uri'])); ?>" rel="shadowbox[gallery]" data-hover-effect="launch-gallery" class="block" data-caption='{
			"": ""
		}'></a>
		<?php } ?>
    </div>
    
	<!--    Used Equipment images  -->
	<!-- width="141" height="104"  -->
	<?php //the images are rendered here and then copied into they target section with JS ?>
	<?php if(!isset($element['#object']->field_used_equipment_images['und'][0]['is_default'])) { ?>
    <ul style="display:none" id="prerenderedImages" class="columns columns-6 children-margin-bottom-2x equipment-gallery">
      <?php $count = 0; ?>
      <?php if (isset($element['#object']->field_used_equipment_videos['und'])): ?>
        <?php foreach ($element['#object']->field_used_equipment_videos['und'] as $delta => $item): ?>
          <?php $youtube_id = vermeer_youtube_parse($item['value']); ?>
          <li class="<?php if(!($count%6)) { echo('column-row'); } ?>" <?php if($count > 5) { echo('data-hide-breakpoint="phablet"'); } ?>>
            <a class="new-window-ignore" href="http://www.youtube.com/embed/<?php echo $youtube_id; ?>" data-hover-effect="black" rel="shadowbox[gallery]">
              <img class="full-width" src="http://img.youtube.com/vi/<?php echo $youtube_id; ?>/0.jpg" class="youtube" rel="<?php echo $youtube_id; ?>" />
            </a>
          </li>
          <?php $count++ ?>
        <?php endforeach; ?>
      <?php endif; ?>
      <?php $imgcount = 0; ?>
      <?php if (!isset($element['#object']->field_used_equipment_images['und'][0]['is_default']) || !isset($element['#object']->field_used_equipment_videos['und'])): ?>
        <?php foreach ($items as $delta => $item): ?>
        	<?php if($imgcount != 0) { ?>
	          <li  class="<?php if(!($count%6)) { echo('column-row'); } ?>"<?php print $item_attributes[$delta]; ?> <?php if($count > 5) { echo('data-hide-breakpoint="phablet"'); } ?>>
	            <a href="<?php echo image_style_url('used_equipment_full', $item['#item']['uri']); ?>" data-hover-effect="black" rel="shadowbox[gallery]">
	              <img class="full-width"  src="<?php echo image_style_url('used_equipment_thumbnail_small', $item['#item']['uri']); ?>" <?php if ($item['#item']['alt']) { echo 'alt="'. $item['#item']['alt'] . '"';} ?> />
	            </a>
	          </li>
			  <?php $count++ ?>
        	<?php } ?>
          <?php $imgcount++ ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </ul>
	<!--
	Update '36' to the number of photos in the gallery.
	If there's only 6 photos, hide third button
	-->
	<?php } ?>
