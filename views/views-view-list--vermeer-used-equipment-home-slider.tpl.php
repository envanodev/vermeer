<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
	
		
		
		<div  class="container-inside">
                <h3 class="heading align-center margin-top-none margin-bottom-2x"><a id="step-used-equipment-carousel"  href="/used-equipment/" class="inherit hover-lighten">Latest Used Equipment</a></h3>
	            <div class="flexslider used-equipment add-overthrow" data-flexslider-theme="black" data-flexslider-arrows="outside" data-flexslider-parameters='{
				"animation": "slide",
				"animationSpeed": 400,
				"slideshow": false,
				"smoothHeight": false,
				"controlNav": false,
				"directionNav": true,
				"useCSS": true,
				"touch": false,
				"prevText": "",
				"nextText": "",
				"itemWidth": 230,
				"minItems": 1,
				"maxItems": 4,
				"start_function": "used_slider_start"
				}'>
<?php  //print $wrapper_prefix; ?>
  <?php if (!empty($title)) { ?>
    <h3 ><?php print $title; ?></h3>
  <?php } ?>
  <?php print $list_type_prefix; ?>
    <?php foreach ($rows as $id => $row) { ?>
      <li class="<?php print $classes_array[$id]; ?>">
	  	<div class="padding-2x left right">
		  <div class="equipment-box">
		  <?php print $row; ?>
		  </div>
	  	</div>
      </li>
    <?php } ?>
  <?php print $list_type_suffix; ?>
<?php //print $wrapper_suffix; ?>
	            </div>
		</div> <!-- .container-inside -->
