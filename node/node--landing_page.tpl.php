<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clear fix"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content['field_intro']); ?>

    <?php
      $type = field_get_items('node', $node, 'field_landing_page_type');
      $view_name = '';
      switch($type[0]['value']) {
        /**
         * News Room
         */
        case 'news_room':
            $view_name = 'news_room';
            $display_id = 'block';
            break;

        /**
         * Special Offers
         */
        case 'special_offers':
            $view_name = 'special_offers';
            if (vermeer_var('special_offers_option') == 0) {
                $display_id = 'block';
            }
            else {
                $display_id = 'block_2';
            }
            break;
        
        /**
         * Locations
         */
        case 'location':
            $view_name = 'location';
            $display_id = 'block';
            break;
        
        /**
         * Find a Sales Rep
         */
        case 'find_sales_rep':
            $view_name = 'find_sales_rep';
            if (vermeer_var('sales_rep_search_option') == 0) {
                $display_id = 'page_1';
                $exposed_block = module_invoke('views', 'block_view', '-exp-find_sales_rep-page_1');
            }
            else {
                $display_id = 'block_1';
            }
            break;

        /**
         * New Equipment
         */
        case 'new_equipment':
            $exposed_block = module_invoke('vermeer_api', 'block_view', 'new_equipment_overview');
            break;

        /**
         * New Equipment
         */
        case 'events_calendar':
            $view_name = 'events';
            $display_id = 'block_1';
            break;
      }
      
      if (isset($exposed_block)) {
        print render($exposed_block);
      }
      if (isset($view_name) && isset($display_id)) {
        print views_embed_view($view_name, $display_id);
      }
    ?>
    <?php print render($content['body']); ?>
  </div>

</div>
